#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ(2.61)
AC_INIT(msc-generator, 8.4, teknos@gmail.com)
AM_INIT_AUTOMAKE([-Wall -W foreign -Wno-portability subdir-objects info-in-builddir])
AM_SILENT_RULES([yes])

# Checks for programs.
AC_PROG_CC
AC_PROG_CXX
AC_PROG_OBJCXX
AC_PROG_RANLIB
AC_PROG_MKDIR_P
AC_ARG_VAR([BISON], [bison command])
AC_CHECK_PROGS([BISON], [bison], [:])
AC_CHECK_PROGS([FCMATCH], [fc-match], [:])
PKG_CHECK_MODULES(CAIRO, cairo >= 1.12.0)
PKG_CHECK_MODULES(GRAPHVIZ, libgvc >= 2.49.0, [AC_DEFINE([GRAPHVIZ_POST_2_49], [1])], [
    PKG_CHECK_MODULES(GRAPHVIZ, libgvc >= 2.36.0)])
PKG_CHECK_MODULES(TINYXML, tinyxml2 >= 8)
PKG_CHECK_MODULES(LIBPNG, libpng >= 1.6)
AC_CHECK_HEADER([glpk.h],
    [AC_CHECK_LIB([glpk], [glp_version],
        [AC_SUBST([GLPK_LIBS], [-lglpk])],
        [AC_MSG_ERROR([libglpk not found])])],
    [AC_MSG_ERROR([glpk.h not found])])

#process flags
AC_ARG_ENABLE([doc], [AS_HELP_STRING([--disable-doc], [skip generating API documentation @<:@default: generate@:>@])], [], [enable_doc=yes])
AS_IF([test "x$enable_doc" = xyes], [AC_MSG_NOTICE([generate API documentation])])
AM_CONDITIONAL([BUILD_DOC], [test "x$enable_doc" = xyes])

AC_ARG_ENABLE([gui], [AS_HELP_STRING([--disable-gui], [don't include the GUI in compilation @<:@default: generate@:>@])], [], [enable_gui=yes])
AS_IF([test "x$enable_gui" = xyes], [
    AC_MSG_NOTICE([compile the GUI])
    PKG_CHECK_MODULES(SDL, sdl2 >= 2.0.0)
    AC_CHECK_HEADER([fontconfig/fontconfig.h], [AC_CHECK_LIB([fontconfig], [FcInit], [has_fontconfig=yes])])
    AS_IF([test "x$has_fontconfig" = xyes], [], [AC_MSG_NOTICE([Fontconfig missing - will not be able to hint font names.])])
])
AM_CONDITIONAL([HAS_FONTCONFIG], [test x$has_fontconfig = xyes])
AM_CONDITIONAL([BUILD_GUI], [test "x$enable_gui" = xyes])

AC_DEFUN([CHECK_FONT], [
    fcout=`$FCMATCH '$1' file`
    font_path=${fcout##:file=}
    font_file=${font_path##*/}
    AS_IF([test "x$font_file" != "x$2"], [AC_MSG_ERROR([need $2 for '$1', not $font_file])])
    AS_IF([test `$FCMATCH '$1' fontversion` != :fontversion=$3], [AC_MSG_ERROR([need version $3 of $2])])
])

AC_ARG_ENABLE([font-checks], [AS_HELP_STRING([--disable-font-checks], [skip tests requiring the canonical fonts @<:@default: check@:>@])], [], [enable_font_checks=yes])
AS_IF([test "x$enable_font_checks" = xyes], [
    AC_MSG_CHECKING([canonical fonts])
    AS_IF([test $FCMATCH = :], [AC_MSG_ERROR([but cannot find 'fc-match'])])
    CHECK_FONT([Nimbus Sans], [NimbusSans-Regular.otf], [65536])
    CHECK_FONT([Nimbus Sans:italic], [NimbusSans-Italic.otf], [72090])
    CHECK_FONT([Nimbus Sans:bold], [NimbusSans-Bold.otf], [65536])
    CHECK_FONT([Nimbus Sans:bold:italic], [NimbusSans-BoldItalic.otf], [72090])
    CHECK_FONT([Droid Sans Fallback:lang=ja], [DroidSansFallbackFull.ttf], [65536])
    CHECK_FONT([DejaVu Sans], [DejaVuSans.ttf], [155320])
    AC_MSG_RESULT([found all])
], [AC_MSG_WARN([skipping checks with canonical fonts])])
AM_CONDITIONAL([MSC_CHECK_FONTS], [test "x$enable_font_checks" = xyes])

AC_CHECK_PROGS([LOFFICE], [libreoffice loffice loimpress simpress], [:])
AS_IF([test $LOFFICE = :], [AC_MSG_WARN([only doing shallow PPTX tests; put `libreoffice' on the PATH if you want full tests])])

# Detect the target system
AC_CANONICAL_HOST
case "${host_os}" in
    linux*)
        build_linux=yes
        ;;
    cygwin*|mingw*)
        build_windows=yes
        ;;
    darwin*)
        build_mac=yes
        ;;
    *)
        AC_MSG_WARN(["OS $host_os may not be supported"])
        ;;
esac
AM_CONDITIONAL([LINUX], [test "$build_linux" = yes])
AM_CONDITIONAL([WINDOWS], [test "$build_windows" = yes])
AM_CONDITIONAL([OSX], [test "$build_mac" = yes])

# for test visualization
AC_CHECK_PROG([WORD_DIFF], [git], [git diff --no-index --word-diff=color --ignore-cr-at-eol -a --exit-code], [diff -auZ])
AC_CHECK_PROGS([AHA], [aha], [:])

AM_EXTRA_RECURSIVE_TARGETS([lang])
AC_SUBST([AM_CXXFLAGS], ["$CAIRO_CFLAGS $GRAPHVIZ_CFLAGS -std=c++20 -Wall -Wextra"])
AC_SUBST([ARFLAGS], [cr])

AC_CONFIG_FILES([
    Makefile
    doc/Makefile
    src/Makefile
    src/libcgencommon/Makefile
    src/libmscgen/Makefile
    src/libgvgen/Makefile
    src/libflow/Makefile
    src/libblock/Makefile
    src/libxxx/Makefile
    src/libgui/Makefile
    src/unix-msc-gen/Makefile
    tests/Makefile
])

dance_around_automake_peeking='Todo.txt:NEWS NEWS:NEWS'
AS_IF([test -f $srcdir/NEWS], [AC_CONFIG_LINKS([${dance_around_automake_peeking}])])

AC_OUTPUT
