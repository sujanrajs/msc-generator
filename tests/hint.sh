#!/bin/bash -e
# Generate hins for input file:
#  MSCGEN=... hint.sh chart-file [chart-type]
[[ "$MSCGEN" ]] || { echo "MSCGEN not defined" >&2; exit 1; }
MSC=$1
SUF=$2
[[ "$SUF" ]] || SUF=${MSC##*.}
lines=(`awk '{t+=length()+1;print t}' $MSC`)
prev=; cur=(1 0); first=(1 0); last=(1 0)
emit(){
 [[ -z "$prev" ]] && return
 echo -n "${first[0]}:${first[1]}"
 if [ ${first[0]} -ne ${last[0]} ] || [ ${first[1]} -ne ${last[1]} ]; then
  echo -n '-'
  [ ${first[0]} -ne ${last[0]} ] && echo -n "${last[0]}:"
  echo -n "${last[1]}"
 fi
 echo ":$prev"
}
for i in `seq $(wc -m $MSC | cut -f1 -d' ')`; do
 hints=`{ echo H$((i-1)); cat $MSC; } | $MSCGEN --tei-mode --nopaths -S $SUF 2>/dev/null |\
  awk 'BEGIN {ORS="\0" } {if(7 == split($0, a, "\001"))print a[1]}' |\
  xargs -0`
 [ "$prev" = "$hints" ] && last=(${cur[*]}) || { emit; prev=$hints; first=(${cur[*]}); last=(${cur[*]}); }
 [[ " ${lines[*]} " =~ " $i " ]] && ((cur[1]=0, ++cur[0])) || ((++cur[1]))
done
emit
