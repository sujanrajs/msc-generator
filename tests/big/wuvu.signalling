msc {
hscale=auto, compress=yes, numbering=yes;

defcolor light_green="205,255,215";
defcolor light_red="250,185,175";
defcolor light_yellow="255,255,160";
defcolor light_blue="171,219,255";

defstyle nodebox [fill.color=lgray, fill.gradient=up, vline.color=lgray];

UE [nodebox, label="\bUE"], 
RGW [nodebox, label="\bRGW\n\sWLAN AP\nNAT\n802.1X authenticator\nDHCP server", fill.color=light_blue], 
AN [nodebox, label="\bAN", fill.color=light_blue], 
BNG [nodebox, label="\bBNG\n\-PEP\nDiameter relay", fill.color=light_blue], 
NASS [nodebox, label="\bNASS\n\-AAA server for BBF", fill.color=light_blue],
dbs_own_ranges [nodebox, label="\bdatabase\nown ranges", line.type=dashed, vline.type=dashed, fill.color=light_blue],
dbs_local_map [nodebox, label="\blocal\nmapping\ndatabase", line.type=dashed, vline.type=dashed, fill.color=light_blue],
PCF [nodebox, label="\bPCF\n\-PDP", fill.color=light_blue], 
VDRA [nodebox, label="\bDRA", fill.color=light_blue], 
PCRF [nodebox, label="\bPCRF"], 
dbs_other_ranges [nodebox, label="\bdatabase\nother's ranges", line.type=dashed, vline.type=dashed],
PDN [nodebox, label="\bPDN-GW\n\-PCEF"], 
AAA [nodebox, label="\bAAA server"];

..:\c(125,125,125) Configuration of own IP address ranges (semi-static, manual) [fill.color=light_yellow, fill.gradient=down] {
    dbs_own_ranges>>dbs_own_ranges: \c(red)add range;
    dbs_own_ranges>>dbs_local_map: "\c(red)dbs store\n\-(IP@x..IP@y, BNG@, PCF@)";
    dbs_own_ranges>>PCF: "\c(red)dbs store\n\-(IPx..IPy, NW id)";
    PCF>>PCRF: \c(red)(3GPP S9) Diameter CCR\n\-(IPx..IPy, NW id);
    PCRF>>dbs_other_ranges: \c(red)dbs store\n\-(IPx..IPy, NW id);
    PCRF>>PCF: \c(red)(3GPP S9) Diamater CCA;
};

..:\c(125,125,125) Local attach and access authentication [fill.color=light_green, fill.gradient=down]{

    UE<->RGW: Setup 802.11 association;
    UE<<>>RGW: EAP;
};

UE->RGW: DHCP Discover;
RGW--RGW: Allocate local IP\naddress for UE;
UE<-RGW: DHCP Offer;
UE->RGW: DHCP Request;
UE<-RGW: DHCP Ack;
UE--UE: Local IPv4@ (CoA)\nreceived;

..:\c(125,125,125) Establish UE-HA SA and do EPC user authentication) [fill.color=light_green, fill.gradient=down]{
    UE<->PDN: IKE_SA_INIT;
    UE->PDN: IKE_AUTH Request\n\-UE Identifier, APN;
    PDN->AAA: Diameter (EAP-RSP/Ident)\n\-UE Identifier, APN;
    AAA->PDN: Diameter (EAP-REQ/AKA-Chall);
    PDN->UE: IKE_AUTH Response;
    UE->PDN: IKE_AUTH Request;
    PDN->AAA: Diameter (EAP-RSP/AKA-Chall);
    AAA->PDN: Diameter (EAP-Success);
    PDN->UE: IKE_AUTH Response;
    UE->PDN: IKE_AUTH Request;
    PDN->UE: IKE_AUTH Response\n\-HoA;
    PDN--PDN: Store mapping\nUE Identifier->RGW@;
    UE--UE: IPv4 home@ (HoA)\nreceived;
 };

UE->PDN: DSMIPv6 Binding update;

..:\c(125,125,125) Establish IP-CAN Session\n (3GPP TS 23.402, 23.203, 29.213) [fill.color=light_red, fill.gradient=down]{
    PDN->PCRF: Diameter CCR\n\-AVPs: UE Identifier (NAI), APN, IP-CAN type,\n UE IP@ (HoA), \c(red)RGW@;
    PCRF--PCRF: Policy decision;
    PCRF->PDN: Diameter CCA\n\-AVPs: Rules, triggers;
    PDN--PDN: Deploy PCC rules\nand event triggers;
};
UE<-PDN: DSMIPv6 Binding ack;

..:\c(125,125,125) Initiate on-demand gateway session [fill.color=light_yellow, fill.gradient=down]{
    PCRF>>dbs_other_ranges: \c(red)dbs query\n\-RGW@, NWid?;
    dbs_other_ranges>>PCRF: \c(red)dbs reply\n\-NW id;
    
    PCRF->VDRA: \c(red) Diameter CCR\n\-AVPs: RGW@, UE Identifier (NAI);
    VDRA>>dbs_local_map: \c(red)dbs query\n\-RGW@, PCF@?;
    dbs_local_map>>VDRA: \c(red)dbs reply\n\-PCF@;
    VDRA->PCF: \c(red) Diameter CCR\n\-AVPs: RGW@, UE Identifier (NAI);

    PCF>>dbs_local_map: \c(red)dbs query\n\-RGW@, BNG@?;
    dbs_local_map>>PCF: \c(red)dbs reply\n\-BNG@;
    PCF--PCF: Store mapping\nRGW@->BNG@;    
    PCF->PCRF: \c(red) Diameter CCA;
};

..:\c(125,125,125) Establish Gateway Control Session\n (3GPP TS 23.402, 23.203, 29.213, ETSI TS 183 060) [fill.color=light_red, fill.gradient=down]{
    PCF--PCF: Decision\nbased on\nroaming agreement;
    PCF->PCRF: (3GPP S9) Diameter CCR\n\-AVPs: Type=initial, UE identifier (NAI), IP-CAN type;
    PCRF--PCRF: Policy decision\ngenerate PCC rules;
    PCRF->PCF: (3GPP S9) Diameter CCA\n\-AVPs: QoS rules, event triggers;
    PCF--PCF: Accept rules\naccording to\nroaming agreement;
    PCF>>PCRF: (3GPP S9) Diameter CCR\n\-AVPs: Rules nACK;
    PCRF>>PCF: (3GPP S9) Diameter CCA\n\-AVPs: Modified QoS rules, event triggers;
    
    PCF->BNG:\c(red) (Radius CoA) CoA-Request\n\-Attributes: UE Identifier (NAI), type=initial,\nQoS rules, event triggers;
    BNG--BNG: Deploy QoS rules\nand event triggers;
    BNG->PCF:\c(red) (Radius CoA) CoA-Ack;
};

{UE<=>RGW-BNG-PDN: User Data (CMIP tunnel) [color=blue];}
{PDN<->: User Data [color=blue];};

..: User data {
    UE->RGW: User data packet\n\-Outer IP: CoA->HA\nOuter UDP: 4500->4500\nInner IP: HoA->peer;
    RGW->BNG: User data packet\n\-Outer IP: RGW->HA\nOuter UDP: p1->4500\nInner IP: HoA->peer;
    BNG--BNG: Apply policy rules;
    BNG->PDN: User data packet\n\-Outer IP: RGW->HA\nOuter UDP: p1->4500\nInner IP: HoA->peer;
    PDN->: User data packet\n\-IP: HoA->peer;
};

}
