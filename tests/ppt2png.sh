#!/bin/bash -e
# LOFFICE=... [REPEAT=3] ppt2png.sh some/path/to/orig.pptx new-filename.png
D=tmp-lo-$RANDOM
REPEAT=${REPEAT:-3}
until E=$($LOFFICE "-env:UserInstallation=file:///$PWD/$D" --convert-to png --outdir $D "$1" |& \
 grep -v 'convert .* using filter : impress_png_Export$' |\
 awk -vx="$1" '$0 {print x ":", $0}') || ((! REPEAT--)); do :; done
F="`basename ${1%.*}`.png"
[[ -f "$D/$F" ]] && mv "$D/$F" "$2" || { [[ "$E" ]] && echo "$E" >&2 || touch "$2"; }
rm -rf $D
