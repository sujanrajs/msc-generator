// dllmain.h : Declaration of module class.

class CMscGeneratorHandlersModule : public ATL::CAtlDllModuleT< CMscGeneratorHandlersModule >
{
public :
	DECLARE_LIBID(LIBID_MscGeneratorHandlersLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_MSCGENERATORHANDLERS, "{947F8EAE-EEF3-4C60-ACF2-244F128C5539}")
};

extern class CMscGeneratorHandlersModule _AtlModule;
