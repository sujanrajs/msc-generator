// dllmain.cpp : Implementation of DllMain.

#include "stdafx.h"
#include "resource.h"
#include "MscGeneratorHandlers_i.h"
#include "dllmain.h"
#include "xdlldata.h"
#include "..\Msc-generator-GUI\MscGenConf.h"

CMscGeneratorHandlersModule _AtlModule;
                    

class CMscGeneratorHandlersApp : public CWinApp
{
public:
    CMscGeneratorHandlersApp() : CWinApp(L"Msc-generator") {}
    CMscGenConf m_config;
// Overrides
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CMscGeneratorHandlersApp, CWinApp)
END_MESSAGE_MAP()

CMscGeneratorHandlersApp theApp;

CMscGenConf *GetConf()
{
    return &theApp.m_config;
}

CEditorBar *GetInternalEditor()
{
    return nullptr;
}

bool IsInternalEditorVisible()
{
    return false;
}


int GetRegistryInt(LPCWSTR key, int def)
{
    return theApp.GetProfileInt(REG_SECTION_SETTINGS, key, def);
}

CStringW GetRegistryString(LPCWSTR key, LPCWSTR def)
{
    return theApp.GetProfileStringW(REG_SECTION_SETTINGS, key, def);
}


BOOL CMscGeneratorHandlersApp::InitInstance()
{
	if (!PrxDllMain(m_hInstance, DLL_PROCESS_ATTACH, NULL))
		return FALSE;

    SetRegistryKey(_T("Zoltan Turanyi"));

    CString msg = m_config.Init();
    if (msg.GetLength())
        MessageBox(nullptr, L"Errors during design library loads:\n"+msg, L"Msc-generator Handler Library", MB_OK);

	return CWinApp::InitInstance();
}

int CMscGeneratorHandlersApp::ExitInstance()
{
	return CWinApp::ExitInstance();
}
 