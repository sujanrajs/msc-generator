/*
This file is part of Msc-generator.
Copyright (C) 2008-2023 Zoltan Turanyi
Distributed under GNU Affero General Public License.

Msc-generator is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Msc-generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file OutputView.h The interface to the view showing error lists.
* @ingroup Msc_generator_files */

#pragma once
#include <vector>
#include "cgencommon.h"
#include "ChartData.h"

/////////////////////////////////////////////////////////////////////////////
// COutputList window

/** The list box used to display errors.
 * The only thin why we specialize CListBox, is to capture double-click events.*/
class COutputList : public CListBox
{
public:
    enum class EMessageType
    {
        ERR,
        WARNING,
        HINT,            //Anything after HINT will be jumped over in a large step
        TECHNICAL_INFO, 
        AUX,
        UNKNOWN
    };
    EMessageType DetermineMessageType(const CString &s) const;
    void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};

/** The dockable pane of the error messages.
 * It has a tab control with two pages eaxch containing one COutputList objects.
 * The two lists correspond to compilation errors or hinted errors.*/
class COutputViewBar : public CDockablePane
{

// Attributes
	CFont m_Font;                                     ///<The font we use to display errors.
protected:
	CMFCTabCtrl	m_wndTabs;                            ///<A tab control hosing the two lists
	COutputList m_wndOutput;                          ///<The list control for compilation errors.
    COutputList m_wndOutputHints;                     ///<The list control for hinted errors.
    std::list<CString> compilation_errors;            ///<A list of compilation error texts (with line num and file name included)
    std::vector<FileLineCol> error_pos;               ///<A (file,line,col) values for compilation errors.
    std::list<CString> compilation_errors_hint;       ///<A list of hinted error texts (with line num included)
    std::vector<FileLineCol> error_pos_hint;          ///<A (file,line,col) values for hinted errors ('file' is ignored if non negative)

protected:
	void AdjusrHorzScroll(CListBox& wndListBox);

// Implementation
public:
    void ShowCompilationErrors(const CDrawingChartData &chart);
    void ShowCshErrors(const std::vector<CString> &errors, 
                       std::vector<FileLineCol> &&err_pos);
    void NextError(bool next, bool small_step);
    std::optional<FileLineCol> GetCurrentErrorLine();
    void JumpToCurrentError(bool silent);
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	DECLARE_MESSAGE_MAP()
};



/**The Saved file List*/
struct SAutoSavedFile
{
    CString disk_name;
    CString display_name;
    CString date_string;
    CTime date;
    SAutoSavedFile(const CString &d_name, CTime time);
    std::string  GenerateListBoxText() const;
    bool operator <(const SAutoSavedFile &o) const { return !(std::tie(date, disk_name)<std::tie(o.date, o.disk_name)); } //sort in decreasing order of date - newest first
};


/** The list box used to display errors.
* The only thin why we specialize CListBox, is to capture double-click events.*/
class CAutoSavedFilesList : public CListBox
{
public:
    std::vector<SAutoSavedFile> m_files; ///<A list of autosaved files - the ones to show
    const unsigned WIDTH = 100;
    StringFormat m_format;
    const SAutoSavedFile *GetFileByIndex(int index);
    CAutoSavedFilesList();
    void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) override;
    void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) override;
    int CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct) override;
    BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult) override;
    void AddFiles(const CString &dir, const LanguageData *lang);
};

/** The dockable pane of the error messages.
* It has a tab control with two pages eaxch containing one COutputList objects.
* The two lists correspond to compilation errors or hinted errors.*/
class CAutoSavedFilesBar: public CDockablePane
{
    friend class CAutoSavedFilesList;

public:
    CAutoSavedFilesList m_wndFiles;    ///<The list control for hinted errors.
    //CButton m_delete_all;
    //CButton m_lang_filter;

protected:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnPaint();
//    afx_msg void OnSetFocus(CWnd* pOldWnd);

    DECLARE_MESSAGE_MAP()
public:
    bool CollectAutoSavedFiles(CString dir, const LanguageData *lang=nullptr);
    bool ShowIfHasEntries(bool force_show = false);
    void DeleteFilesAndHide();
};

