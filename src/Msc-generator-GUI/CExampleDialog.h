#pragma once
#include "afxdialogex.h"


// CExampleDialog dialog

class CExampleDialog : public CDialog
{
	DECLARE_DYNAMIC(CExampleDialog)

public:
	CExampleDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CExampleDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EXAMPLE_DIALOG };
#endif

protected:
	std::vector<Example*> filtered_examples;

	virtual BOOL OnInitDialog();
	afx_msg void OnSearchChanged(); ///<Called when the user changes the filtering text
	afx_msg void OnSelChanged();    ///<Called when the selected example changes
	afx_msg void OnTextChanged();   ///<Called when the text of the example changes (by user typing or selecting a new example)

	DECLARE_MESSAGE_MAP()
};
