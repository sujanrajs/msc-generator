// CExampleDialog.cpp : implementation file
//

#include "stdafx.h"
#include "afxdialogex.h"
#include "MscGenConf.h"
#include "Msc-generator.h"
#include "CExampleDialog.h"
#include "resource.h"
#include "utf8utils.h"
#include "graphchart.h"
#include "blockchart.h"
#include "msc.h"

// CExampleDialog dialog

IMPLEMENT_DYNAMIC(CExampleDialog, CDialog)

CExampleDialog::CExampleDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_EXAMPLE_DIALOG, pParent)
{

}

CExampleDialog::~CExampleDialog()
{
}

BEGIN_MESSAGE_MAP(CExampleDialog, CDialog)
    ON_LBN_SELCHANGE(IDC_EXAMPLE_LIST, &CExampleDialog::OnSelChanged)
    ON_EN_CHANGE(IDC_EXAMPLE_SEARCH, &CExampleDialog::OnSearchChanged)
    ON_EN_CHANGE(IDC_EXAMPLE_EDITOR, &CExampleDialog::OnTextChanged)
END_MESSAGE_MAP()


BOOL CExampleDialog::OnInitDialog() {
    const BOOL ret = CDialog::OnInitDialog();
    OnSearchChanged(); //fill in listbox
    auto* lb = ((CListBox*)GetDlgItem(IDC_EXAMPLE_LIST));
    lb->SetCurSel(0);
    auto* const editor = ((CRichEditCtrl*)GetDlgItem(IDC_EXAMPLE_EDITOR));
    editor->SetEventMask(ENM_CHANGE);
    OnSelChanged(); //fill in chart text and graphics
    SetWindowPos(&this->wndTop, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
    CMscGenConf* const pConf = GetConf();
    SetWindowText(AsUnicode(pConf->m_lang->description)+L" Examples");
    editor->SetFocus();
    return ret;
}



void CExampleDialog::OnSearchChanged() {
    auto* const lb = ((CListBox*)GetDlgItem(IDC_EXAMPLE_LIST));
    const int sel_before = lb->GetCurSel();
    const Example* const sel_example_before = 0<=sel_before && sel_before <filtered_examples.size()
        ? filtered_examples[sel_before] : nullptr;
    filtered_examples.clear();
    CEdit* const search = (CEdit*)GetDlgItem(IDC_EXAMPLE_SEARCH);
    CString filter;
    search->GetWindowTextW(filter);
    CMscGenConf* const pConf = GetConf();
    const std::string filter_utf8 = ConvertFromUTF16_to_UTF8(std::wstring_view(filter.GetString(), filter.GetLength()));
    filtered_examples = pConf->m_examples->Filter(filter_utf8, pConf->m_lang);
    lb->ResetContent();
    for (Example* e : filtered_examples)
        lb->AddString(AsUnicode(e->name));
    const auto i = std::ranges::find(filtered_examples, sel_example_before);
    const int new_sel = i==filtered_examples.end() ? -1 : i-filtered_examples.begin();
    lb->SetCurSel(new_sel);
}

void CExampleDialog::OnSelChanged() {
    auto* const lb = ((CListBox*)GetDlgItem(IDC_EXAMPLE_LIST));
    const int sel = lb->GetCurSel();
    if (sel<0 || filtered_examples.size()<=sel) return;
    Example* const sel_example = filtered_examples[sel];

    auto* const editor = ((CRichEditCtrl*)GetDlgItem(IDC_EXAMPLE_EDITOR));
    editor->SetRedraw(false);
    editor->SetWindowText(ConvertFromUTF8_to_UTF16(sel_example->text).c_str());
    auto* pEditorBar = GetInternalEditor();
    editor->SetSel(0, -1); //all
    editor->SetFont(pEditorBar->m_Font.get());
    editor->SetSel(0, 0); //none
    editor->SetRedraw(true);
    editor->Invalidate();

    auto* const desc = ((CEdit*)GetDlgItem(IDC_EXAMPLE_DESCRIPTION));
    desc->SetWindowTextW(AsUnicode(sel_example->explanation));
}

void CExampleDialog::OnTextChanged() {
    constexpr bool pedantic = false;
    CMscGenConf* const pConf = GetConf();
    auto* const lb = ((CListBox*)GetDlgItem(IDC_EXAMPLE_LIST));
    const int sel = lb->GetCurSel();
    if (sel<0 || filtered_examples.size()<=sel) return;
    Example* const sel_example = filtered_examples[sel];

    //Save the edited text back to the example
    auto* const editor = ((CRichEditCtrl*)GetDlgItem(IDC_EXAMPLE_EDITOR));
    CString text;
    editor->GetWindowText(text);
    text.Remove(L'\r');
    sel_example->text = ConvertFromUTF16_to_UTF8(std::wstring_view(text.GetString(), text.GetLength()));

    //Colorize
    const auto csh = pConf->m_lang->pCsh->Clone();
    csh->FileName = sel_example->name;
    csh->ParseText(std::string(sel_example->text), 0, pedantic);
    csh->CshList.SortByPos();
    CHARRANGE cr;
    editor->GetSel(cr);
    editor->SetEventMask(0);
    editor->SetRedraw(false);
    for (const CshEntry& e : csh->CshList)
        if (e.color<COLOR_MAX) {
            editor->SetSel(e.first_pos-1, e.last_pos);
            editor->SetSelectionCharFormat(pConf->m_csh_cf[csh->params->color_scheme][e.color]);
        }
    editor->SetSel(cr); //Whatever was before
    editor->SetEventMask(ENM_CHANGE);
    editor->SetRedraw(true);
    editor->Invalidate();
    editor->RedrawWindow();

    //Compile the chart
    std::unique_ptr<Chart> chart = pConf->m_lang->create_chart(nullptr, nullptr);
    if (chart) {
        chart->prepare_for_tracking = false;
        chart->prepare_element_controls = false;
        //add shapes
        chart->Shapes = pConf->m_lang->shapes;
        //add errors
        chart->Error = pConf->m_lang->designlib_errors;
        //add designs
        chart->ImportDesigns(pConf->m_lang->designs);
        //set copyright text
        chart->copyrightText = pConf->m_CopyrightText;

        //MscChart specific settings
        if (msc::MscChart* msc = dynamic_cast<MscChart*>(chart.get())) {
            //copy pedantic flag from app settings
            msc->mscgen_compat = EMscgenCompat::AUTODETECT;
            msc->Error.warn_mscgen = true;
        }
        //Graph specific settings
        if (graph::GraphChart* graph = dynamic_cast<GraphChart*>(chart.get()))
            graph->do_orig_lang = false;
        chart->SetPedantic(pedantic);

        try {
            chart->GetProgress()->StartParse();
            chart->ParseText(sel_example->text, sel_example->name);
            if (!chart->Error.hasFatal())
                //Do postparse, compile, calculate sizes and sort errors by line
                chart->CompleteParse(/* autopaginate =*/ false,
                                     false, XY(0, 0), false, true);
            chart->Error.RemovePathFromErrorMessages();
            chart->GetProgress()->Done();
        } catch (const AbortCompilingException&) {
        }
    }
    CStatic* const bitmap_ctrl = (CStatic*)GetDlgItem(IDC_EXAMPLE_PICTURE);
    CRect bitmap_ctrl_rect;
    bitmap_ctrl->GetWindowRect(bitmap_ctrl_rect);
    const double fit_scale = chart
        ? std::min(bitmap_ctrl_rect.Width()/chart->GetTotal().x.Spans(),
                   bitmap_ctrl_rect.Height()/chart->GetTotal().y.Spans())
        : 1.0;
    const double scale = std::min(std::max(fit_scale, 0.3), 2.0);
    const XY size = chart ? chart->GetTotal().Spans()*scale : XY(10, 10);
    CBitmap bitmap, * pOldBmp;
    CDC* pDC = GetDC();
    CDC MemDC;
    MemDC.CreateCompatibleDC(pDC);
    bitmap.CreateCompatibleBitmap(pDC, (int)size.x, (int)size.y);
    pOldBmp = MemDC.SelectObject(&bitmap);
    if (chart) {
        Canvas canvas(Canvas::WIN, MemDC, chart->GetTotal(),
                      chart->GetCopyrightTextHeight(), XY(scale, scale));
        chart->DrawComplete(canvas, true, 0);
    }
    MemDC.SelectObject(pOldBmp);
    ReleaseDC(&MemDC);
    ReleaseDC(pDC);

    bitmap_ctrl->SetBitmap(bitmap);

}