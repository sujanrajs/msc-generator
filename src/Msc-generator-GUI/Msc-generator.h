/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2023 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file Msc-generator.h Main header file for the Msc-generator application
 * @ingroup Msc_generator_files */

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "ChartData.h"
#include "OutputView.h"
#include "MiniEditor.h"
#include "MscGenDoc.h"
#include "MscGenConf.h"
#include "cgencommon.h"
#include "canvas.h"



class CMscGenApp : public CWinAppEx
{
public:
	CMscGenApp();
    /** Get our one and only document */
    CMscGenDoc *GetDoc(void);

// Overrides
public:
	virtual BOOL InitInstance();
    int ExitInstance();

// Implementation
	COleTemplateServer m_server;                 ///<Server object for document creation
	UINT               m_nAppLook;               ///<The visual style of the Ribbon and other MFC components
	COutputViewBar    *m_pWndOutputView;         ///<Shortcut to the error window
	CEditorBar        *m_pWndEditor;             ///<Shortcut to the internal editor
    CMscGenConf        m_config;                  ///<Config data

//Status
	bool m_bFullScreenViewMode; ///<True if we were opened by "ViewFullScreen OLE verb"

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

    /** Show the language selector dialog and set the selected language. Returns true if a change has been made*/
    bool AskForLanguage();
    void UpdatePrinterData();

    CDataRecoveryHandler *GetDataRecoveryHandler() override;

    /** @name GUI control functions
     * These functions are called when a control is invoked on the GUI. */
    /** @{ */
	afx_msg void OnAppAbout();
    afx_msg void OnHelp();
    afx_msg void OnExamples();
    afx_msg void OnFileNew();
    afx_msg void OnFileOpen();
    afx_msg void OnFilePrintSetup();
    afx_msg void OnUpdatePrintSetup(CCmdUI *pCmdUI);
	afx_msg void OnEditPreferences();
    afx_msg void OnButtonDefaultText();
    afx_msg void OnUpdateButtonDefaultText(CCmdUI *pCmdUI);
    afx_msg void OnCheckPedantic();
    afx_msg void OnUpdateCheckPedantic(CCmdUI *pCmdUI);
    afx_msg void OnCheckWarnings();
    afx_msg void OnUpdateCheckWarnings(CCmdUI *pCmdUI);
    afx_msg void OnCheckFullPath();
    afx_msg void OnUpdateCheckFullPath(CCmdUI *pCmdUI);
    afx_msg void OnCheckPageBreaks();
    afx_msg void OnUpdateCheckPageBreaks(CCmdUI *pCmdUI);
    afx_msg void OnComboCsh();
    afx_msg void OnUpdateComboCsh(CCmdUI *pCmdUI);
    afx_msg void OnCheckSmartIndent();
    afx_msg void OnCheckTABIndents();
    afx_msg void OnUpdateCheckSmartIndent(CCmdUI *pCmdUI);
    afx_msg void OnCheckCshError();
    afx_msg void OnUpdateCheckTABIndents(CCmdUI *pCmdUI);
    afx_msg void OnUpdateCheckCshError(CCmdUI *pCmdUI);
    afx_msg void OnCheckCshErrorInWindow();
    afx_msg void OnUpdateCheckCshErrorInWindow(CCmdUI *pCmdUI);
    afx_msg void OnUpdateEditPreferences(CCmdUI *pCmdUI);
    afx_msg void OnCheckEeOther();
    afx_msg void OnCheckEeNotepad();
    afx_msg void OnCheckEeNotepadpp();
    afx_msg void OnUpdateCheckEeOther(CCmdUI *pCmdUI);
    afx_msg void OnUpdateCheckEeNotepad(CCmdUI *pCmdUI);
    afx_msg void OnUpdateCheckEeNotepadpp(CCmdUI *pCmdUI);
    afx_msg void OnCheckHints();
    afx_msg void OnCheckSmartHintCompact();
    afx_msg void OnComboSmartHintFilter();
    afx_msg void OnUpdateComboSmartHintFilter(CCmdUI* pCmdUI);
    afx_msg void OnCheckSmartHintLineStart();
    afx_msg void OnCheckSmartHintEntity();
    afx_msg void OnCheckSmartHintEscape();
    afx_msg void OnCheckSmartHintAttrName();
    afx_msg void OnCheckSmartHintKeywordMarker();
    afx_msg void OnCheckSmartHintAttrValue();
    afx_msg void OnUpdateCheckSmartHintBoxes(CCmdUI *pCmdUI);
    afx_msg void OnUpdateCheckHints(CCmdUI *pCmdUI);
    afx_msg void OnButtonTrackColor();
    afx_msg void OnEmbeddedoptionsFallbackRes();
    afx_msg void OnAutoPaginate();
    afx_msg void OnUpdateAutoPaginate(CCmdUI *pCmdUI);
    afx_msg void OnOriginalEngine();
    afx_msg void OnUpdateOriginalEngine(CCmdUI *pCmdUI);
    afx_msg void OnAutoHeaders();
    afx_msg void OnUpdateAutoHeaders(CCmdUI *pCmdUI);
    afx_msg void OnComboScale();
    afx_msg void OnButtonPages();
    afx_msg void OnComboAlignment();
    afx_msg void OnEditMarginL() { DoEditMargin(ID_EDIT_MARGIN_L); }
    afx_msg void OnEditMarginR() {DoEditMargin(ID_EDIT_MARGIN_R);}
    afx_msg void OnEditMarginT() {DoEditMargin(ID_EDIT_MARGIN_T);}
    afx_msg void OnEditMarginB() {DoEditMargin(ID_EDIT_MARGIN_B);}
            void DoEditMargin(UINT id);
    afx_msg void OnUpdatePrintPreviewEdits(CCmdUI *pCmdUI);
            void UpdateMscgenCompatPane();
    afx_msg void OnComboMscgenCompat();
    afx_msg void OnUpdateComboMscgenCompat(CCmdUI *pCmdUI);
    afx_msg void OnCheckMscgenCompatWarn();
    afx_msg void OnUpdateCheckMscgenCompatWarn(CCmdUI *pCmdUI);
    afx_msg void OnCheckAskLanguage();
    afx_msg void OnUpdateCheckAskLanguage(CCmdUI *pCmdUI);
    afx_msg void OnCheckAutosave();
    afx_msg void OnUpdateCheckAutosave(CCmdUI *pCmdUI);
    afx_msg void OnEditAutosaveInterval();
    afx_msg void OnUpdateEditAutosaveInterval(CCmdUI *pCmdUI);
    afx_msg void OnCheckOpenRecoveryAtStartup();
    afx_msg void OnUpdateCheckOpenRecoveryAtStartup(CCmdUI *pCmdUI);
    afx_msg void OnCheckFilterAutosave();
    afx_msg void OnUpdateCheckFilterAutosave(CCmdUI *pCmdUI);
    afx_msg void OnButtonDeleteAutosavedFiles();
    afx_msg void OnUpdateButtonDeleteAutosavedFiles(CCmdUI *pCmdUI);
    afx_msg void OnCheckRecoveryOverwrite();
    afx_msg void OnUpdateCheckRecoveryOverwrite(CCmdUI *pCmdUI);
    afx_msg void OnCheckRecoveryInsertAtCursor();
    afx_msg void OnUpdateCheckRecoveryInsertAtCursor(CCmdUI *pCmdUI);
    afx_msg void OnCheckRecoveryDelete();
    afx_msg void OnUpdateCheckRecoveryDelete(CCmdUI *pCmdUI);
    afx_msg void OnCheckWarnCompilationTime();
    afx_msg void OnUpdateCheckWarnCompilationTime(CCmdUI *pCmdUI);
    afx_msg void OnCheckCurrentLineHighlight();
    afx_msg void OnUpdateCheckCurrentLineHighlight(CCmdUI *pCmdUI);
    afx_msg void OnCheckShowLineNums();
    afx_msg void OnUpdateCheckShowLineNums(CCmdUI *pCmdUI);
    /** @} */
    DECLARE_MESSAGE_MAP()
    afx_msg void OnCheckSkipCopyright();
    afx_msg void OnUpdateCheckSkipCopyright(CCmdUI *pCmdUI);
};

extern CMscGenApp theApp;

/** This class helps recovering at failure.
* Redefined to override GetDocumentListName(). */
class CMscGenDataRecoveryHandler : public CDataRecoveryHandler
{
    using CDataRecoveryHandler::CDataRecoveryHandler;
public:
    virtual BOOL Initialize() override;
    virtual CString GetDocumentListName(_In_ CDocument *pDocument) const override;
    CString CurrentAutosaveName() const { return m_mapDocNameToAutosaveName.GetSize() ?  m_mapDocNameToAutosaveName.PGetFirstAssoc()->value : CString(_T("")); }
};


