/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2023 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file PopupList.h The interface for the list box & its container 
 * window popping up to offer hints. 
 * @ingroup Msc_generator_files */
#pragma once

#include "stdafx.h"
#include "resource.h"
#include "stringparse.h"
#include "csh.h"

/** An object used to store hint box elements for the index of the bitmap cache.*/
struct HintListBoxItemCacheElement
{
    string label;
    EHintItemSelectionState state;
    CshHintGraphicCallback callback;
    CshHintGraphicParam param;
    HintListBoxItemCacheElement(const string &l, EHintItemSelectionState s,
                                CshHintGraphicCallback c, CshHintGraphicParam p) :
                                label(l), state(s), callback(c), param(p) {}
    bool operator < (const HintListBoxItemCacheElement &o) const {
        return state < o.state ? true : state  > o.state ? false :
               callback < o.callback ? true : callback > o.callback ? false :
               param < o.param ? true : param > o.param ? false :
               label < o.label;
    }
};

/** A collection holding hints mapped to bitmaps, so we need to
 * draw a hint only once.*/
typedef std::map<HintListBoxItemCacheElement, std::unique_ptr<CBitmap>> HintListBoxItemCache;


/** An owner-drawn list box containing a list of hints.*/
class CHintListBox : public CListBox 
{
protected:
    DECLARE_MESSAGE_MAP()
    BOOL OnToolTipText(UINT id, NMHDR * pNMHDR, LRESULT * pResult);
    double GetCurrentDPIScalingFactor();
    std::string m_fontname;
    double m_fontsize;
    double m_zoom;
    void RegenerateFormat();
public:
    CSize             m_current_size; ///<The current geometry of us.
    double            m_zoom_hintpic; ///<How much to scale the hint pics due to font sizes (always >1)
    StringFormat      m_format;       ///<Default format for hints
    int               m_cur_sel;      ///<The index of the currently selected item
    CshHintStore      m_csh_store;    ///<Stores the current set of hints and shapes
    CMFCToolTipCtrl   m_tooltip;      ///<The tooltip showing hint descriptors
    HintListBoxItemCache m_cache;     ///<We store pre-drawn box items here.
    CHintListBox();
    /** Potential result of processing the hints. */
    enum EHintProcResult {
        HINTS_REPLACE,     ///<After chain forwarding, we determine, we need to replace the string under cursor (only one result and user pressed Ctrl+Space)
        HINTS_CHANGED,     ///<The list of hints changed
        HINTS_NOT_CHANGED, ///<The list of hints remained as before
    };

    EHintProcResult PreprocessHints(Csh &csh, const std::string &uc, 
                                    bool userRequest, bool inhintmode, double zoom);
    CSize SetHintsToCurrent();
    void SetStringUnderCursor(const char *uc);
    void UpDownKey(int offset);
    void ChangeSelectionTo(int index, EHintItemSelectionState state);
    void ChangeSelectionTo(int index);
    /** Return a pointer to the hint currently selected. nullptr if nothing selected.*/
    const CshHint* GetSelectedHint() const {return GetHintByNum(m_cur_sel);}
    const CshHint* GetHintByNum(int i) const;
    const CshHint* GetHintByData(DWORD_PTR itemData) const;
    const CshHint* GetHintByDataAsString(DWORD_PTR itemData) const;
    virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT /*lpMeasureItemStruct*/);
    virtual int CompareItem(LPCOMPAREITEMSTRUCT /*lpCompareItemStruct*/);
    void SelectNewFont(const CString &font, unsigned size10);
};

class CCshRichEditCtrl;

// CPopupList dialog

/** A dialog window having a single list.*/
class CPopupList : public CDialog
{
	DECLARE_DYNAMIC(CPopupList)

public:
	CPopupList(CWnd* pParent, CCshRichEditCtrl* pEditCtrl);   // standard constructor
    virtual ~CPopupList() {}

    CCshRichEditCtrl *m_pEditCtrl; ///<The internal editor (for setting focus and replacing)
// Dialog Data
	enum { IDD = IDD_POPUPLIST };

    void Show(const LPCSTR uc, int x, int y);
    void Hide();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CHintListBox m_listBox;               ///<The listbox containing the hints
    bool         m_shown;                 ///<Whether we are showing
    /** Called when the user dblclicks an item.*/
    afx_msg void OnLbnDblclkList2();      
    /** Called when the user cancels the selection in the listbox.*/
    afx_msg void OnLbnSelcancelList2();
    /** Called when the user changes the selection in the listbox.*/
    afx_msg void OnLbnSelchangeList2();
};
