/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2023 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file MiniEditor.h The internal editor: CEditorBar and CCshRichEditCtrl classes.
* @ingroup Msc_generator_files */

#pragma once
#undef min
#undef max
#include "msc.h"
#include "msccsh.h"
#include "PopupList.h"
#include <utility>

/////////////////////////////////////////////////////////////////////////////
// CMiniEditor window


class CEditorBar;

class CScrollingRichEditCtrl : public CRichEditCtrl
{
protected:
    DECLARE_MESSAGE_MAP()
public:
    afx_msg BOOL CWndOnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
        { return CWnd::OnMouseWheel(nFlags, zDelta, pt); }
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};

/** A color syntax highlighting and smart indenting editor control
 * that is doing hinting.*/
class CCshRichEditCtrl : public CScrollingRichEditCtrl
{
protected:
    DECLARE_MESSAGE_MAP()
    /** Need to redefine this because default behaviour skips linefeeds
     * if they are the last character of the text to paste.*/
    LRESULT OnPasteSpecial(WPARAM wParam, LPARAM lParam);

private:
    bool m_bUserRequested;       ///<True if the the current hints session was invoked via Ctrl+Space
    bool m_bTillCursorOnly;      ///<True if the the current hints session started at the beginning of a word.
    CEditorBar * const m_parent; ///<Our parent, an editor bar.
    CHARRANGE m_crSel_before;    ///<The selection before a change
    POINT m_scroll_pos_before;   ///<The scrollbar position before a change
    std::string m_prev_text;     ///<The previous text, for which m_csh is relevant. Used in UpdateCSH().
    bool m_bRedrawState;         ///<Stores what did we last set SetRedraw(), since we have no GetRedraw()
    /** A type to store redraw and doc notification status.*/
    struct WindowUpdateStatus
    {
        bool redraw_enabled;      ///<True if redraw is enabled (see CWnd::SetRedraw())
        bool doc_updates_enabled; ///<True if we send change and selection change notifications to CMscGenDoc
    };
    /** How tall is one character in pixels */
    long CharHeight() const;
public:
    /** Modes of updating CSH */
    enum UpdateCSHType {
        HINTS_AND_LABELS = 0, ///<Do not update colory syntax highlighting, just parse text for hints and the location of labels (for smart indenting)
        CSH = 1,              ///<Update color sytnax highlighting (and collect hints and label positions, as well)
        FORCE_CSH = 2         ///<Force an update of color sytnax highlighting (and collect hints and label positions, as well)
    };
    int m_tabsize;      ///<The default size for a tabulator. Used to replace '\\t's and when not in smart indent mode
    POINT m_ctxMenuPos; ///<When user clicks the context menu, we save the position in RichEdit client coordinates
    CPopupList m_hintsPopup;     ///<A List box that pops up for hint sessions.
    /**Init members*/
	CCshRichEditCtrl(CEditorBar *parent);
    /**Create Windows object and show.*/
    virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);
    afx_msg void OnPaint();

    /** @name Generic helpers
    * @{ */
    void SetUtf8Text(const char *utf8);
    std::string GetUtf8TextAsStdString() const;
    CStringA GetUtf8TextAsCStringA() const;
    void ReplaceSelUtf8(const char *utf8, bool bCanUndo = true);
    void GetTextRangeUtf8(long start, long end, CStringA &str);
    void SetUnicodeText(const CStringW &wstring);
    void GetUnicodeText(CStringW &wstring) const;
    void GetSelLineCol(int &lStartLine, int &lStartCol, int &lEndLine, int &lEndCol) const;
	int  GetLineString(int line, CStringA &str);
    /**Convert a line,col position to a RichEditCtrl position.*/
	long ConvertLineColToPos(unsigned line, unsigned col) const {return LineIndex(line) + col;}
    /**Convert a RichEditCtrl position to a line,col position.*/
	void ConvertPosToLineCol(long pos, int &line, int &col) const {line=LineFromChar(pos); col=pos-LineIndex(line);}
    /** Get the column part of a position */
    long ConvertPosToCol(long pos) { return pos-LineIndex(LineFromChar(pos)); }
    /** Move the cart to a position */
	void JumpToLine(int line, int col);
    /** Get the char pos of the context menu or (in its absence) the start of the selection. */
    long GetMenuCharPos() const;
    /** Get the line, col of the context menu or (in its absence) the start of the selection. */
    std::pair<int, int> GetMenuLineCol() const;
    //@}
    /** @name Functions for intelligent indentation
    * @{ */
    int  CalcTabStop(int col, bool forward, int smartIndent=-1, int prevIndent=-1, bool strict=false);
    bool SetCurrentIndentTo(int target_indent, int current_indent, int col, long lStart, long lEnd, bool standalone);
    bool ReIndent(int from_line, int to_line);
    //@}

    BOOL PreTranslateMessage(MSG* pMsg);
    WindowUpdateStatus GetWindowUpdate();
    WindowUpdateStatus DisableWindowUpdate();
    void RestoreWindowUpdate(const WindowUpdateStatus &s);
    /** @name Color Syntax Highlighting functions
    * @{ */
    void UpdateText(const char *text, const CHARRANGE &cr, const POINT &scr);
    void UpdateText(const char *text, int lStartLine, int lStartCol, int lEndLine, int lEndCol, const POINT &scr);
    bool UpdateCSH(UpdateCSHType updateCSH);
    void CancelPartialMatch();
    void ReCsh();
    /** @name Hint window related
      * @{ */
    /** Shows popup hint window and enters hint mode.
     * Assumes we have called UpdateCSH() just before and m_csh is up-to-date.
     * Also used to update hints in popup, if already in hint mode.
     * @param [in] setUptoCursor If true, we will consider the hinted word
     *                           to last only up to the cursor position, even
     *                           if there are alphanum chars after.
     *                           (Set when hint mode was triggered at the beginning of a word.)*/
    void StartHintMode(bool setUptoCursor); 
    /**Returns true if we are in hint mode*/
    bool InHintMode() const {return m_hintsPopup.m_shown;}
    /**Hides the hint popup and leaves hint mode. Text is not changed.*/
    void CancelHintMode();
    /** Unset the user selected nature of the hint session. Called if after an update of
     * the text the cursor has ended up in a different word.*/
    void CancelUserSelected() {m_bUserRequested = false;}
    /**Replace the hinted string of the text with the substitute.
     * Called when we select a hint from the list. 
     * We remain in hint mode if the user hit a '.' dot or if
     * the originator of the hint indicated so (like a grouped hint).*/
    void ReplaceHintedString(const CshHint *hint);
    /** @} */
    /** @name Entity rename related
      * @{ */
    /** Gets any entity name that is at the place where we clicked
     * the contetx menu in the internal editor.
     * Returns empty if a) no internal editor context menu clicked;
     * or b) no entity name at the clicked pos.
     * Also returns which character within the returned string 
     * did the user click on.*/
    std::pair<std::string_view, size_t> GetEntityNameAtMenu() const;
    /** Called when the user selects "Rename this entity" in internal editor. */
    afx_msg void OnRenameEntity();
    /** @} */
};

class CMainFrame;
class CEditorBar;

class CPaneSplitter : public CSplitterWndEx
{
protected:
    DECLARE_MESSAGE_MAP()
    CEditorBar * const m_parent;
    unsigned m_width; ///<How wide the linenums shall be
public:
    CPaneSplitter(CEditorBar *p);
    afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnSelChange(UINT id, NMHDR*pNotifyStruct, LRESULT*result);
    afx_msg BOOL OnCommand(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnNcHitTest(CPoint point) { return HTNOWHERE; }
    void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);
    /** Change zoom. Called if the mouse wheel was rolled inside our area.
     * We zoom/scroll both text editors.*/
    BOOL DoMouseWheel(UINT nFlags, short zDelta, CPoint pt);
    void UpdateLineWindowWidth(unsigned numChar);
};

/** A custom dockable pane hosting a CCshRichEditCtrl.*/
class CEditorBar : public CDockablePane
{

public:
	CEditorBar(); ///< Construction

    CPaneSplitter m_wndSplitter;              ///<The splitter we have
    CScrollingRichEditCtrl m_ctrlLinenum;     ///<The line numbers
    CCshRichEditCtrl m_ctrlEditor;            ///<The edit control
    unsigned m_linesNumbered;                 ///<The last line we have line numbers shown for
    std::unique_ptr<CFont> m_Font;            ///<Font to use
    std::unique_ptr<CFindReplaceDialog>
        m_pFindReplaceDialog;                 ///<The search/replace dialog
	CString m_sLastFindString;                ///<The last string to search for
	CString m_sLastReplaceString;             ///<The last string to replace to
	bool m_bLastMatchCase;                    ///<If we have 'match case' on the last time we searched
    bool m_bLastMatchWholeWord;               ///<If we have 'whole word' on the last time we searched
	CPoint m_ptLastFindPos;                   ///<Last screen position of the text found.
	long m_lInitialSearchPos;                 ///<Text pos where we started searching from
	BOOL m_bFirstSearch;                      ///<True if this is the first search in a search/replace session
	bool m_bSuspendNotifications;             ///<True if we shall not notify the document about a change in selection (e.g., during coloring)
    long m_totalLenAtPreviousSelChange;       ///<The length of the text at the last selection change (We use this to test if we have just changed the selection or the text, as well.)
    std::pair<long, long> m_lastline;         ///<The last line the cursor was in (bold).
    int m_FontPixelHeight;                    ///<The height of the font in pixels on screen
    double m_zoom;                            ///<Current zoom.
public:
	virtual ~CEditorBar();
	void SetReadOnly(bool=true);
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
    LRESULT OnFindReplaceMessage(WPARAM wParam, LPARAM lParam);
    BOOL OnShowControlBarMenu(CPoint pt);
    void OnPressCloseButton() override;
    DECLARE_MESSAGE_MAP()
public:
    void SelChanged();
    void UpdateLineNumberHighlight();
    void UpdateNumbers(bool char_width_change, bool selection_change);
    static void FillLogFont(LOGFONT &lf, const CString &font, unsigned size10, BYTE CharSet, bool FixedPitchOnly);
    void SelectNewFont(const CString &font, unsigned size, BYTE CharSet, bool FixedPitchOnly);

	afx_msg void OnEditFindReplace(bool findOnly);
	afx_msg void OnEditRepeat();

	bool UpdateLastFindStringFromSelection();
	BOOL SameAsSelected(LPCTSTR lpszCompare, BOOL bCase);
	void AdjustDialogPosition(CDialog* pDlg);
	BOOL FindText(LPCTSTR lpszFind, BOOL bCase = TRUE, BOOL bWord = TRUE, BOOL bNext = TRUE);
	long FindAndSelect(DWORD dwFlags, FINDTEXTEX& ft);
	void TextNotFound(LPCTSTR lpszFind);
    void SelectAll(); 
};
