/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2023 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file MscGenDoc.cpp The main document class with two dialogs needed for export parameters.
* @ingroup Msc_generator_files */

#include "stdafx.h"
#include "MscGenConf.h"

#ifndef SHARED_HANDLERS
#include "Msc-generator.h"
#include "MainFrm.h"
#endif

#include "version.h"
#include "utf8utils.h"
#include "MscGenDoc.h"
#include "SrvrItem.h"
#include "MscGenView.h"
#include "commandline.h" //for SplitLoadData and PackLoadData
#include "msc.h"

#include <string>
#include <list>
#include <set>
#include <map>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/** The dialog box asking about scaling when exporting to a bitmap.*/
class CScaleDlg : public CDialog
{
public:
	CScaleDlg();

// Dialog Data
	enum { IDD = IDD_SCALE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog( );
    /** Called when a button is clicked. */
	afx_msg void OnBnClicked();

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
    double m_scale;    ///<What scale to use (linked to a control in the dialog)
    UINT m_x;          ///<What x size to use (linked to a control in the dialog)
    UINT m_y;          ///<What y size to use (linked to a control in the dialog)
    UINT m_xy_x;       ///<What xy x to use (linked to a control in the dialog)
    UINT m_xy_y;       ///<What xy y to use (linked to a control in the dialog)
    CSize m_orig_size; ///<The original size(linked to a control in the dialog)
    double m_res_x;    ///<What x resolution to use (linked to a control in the dialog)
    double m_res_y;    ///<What y resolution to use (linked to a control in the dialog)
    int m_selected;    ///<Which method is selected.
};

BEGIN_MESSAGE_MAP(CScaleDlg, CDialog)
	ON_BN_CLICKED(IDC_RADIO_SCALING_NO, &CScaleDlg::OnBnClicked)
	ON_BN_CLICKED(IDC_RADIO_SCALING_SCALE, &CScaleDlg::OnBnClicked)
	ON_BN_CLICKED(IDC_RADIO_SCALING_X, &CScaleDlg::OnBnClicked)
	ON_BN_CLICKED(IDC_RADIO_SCALING_Y, &CScaleDlg::OnBnClicked)
	ON_BN_CLICKED(IDC_RADIO_SCALING_XY, &CScaleDlg::OnBnClicked)
END_MESSAGE_MAP()

CScaleDlg::CScaleDlg() : CDialog(CScaleDlg::IDD)
    , m_scale(0)
    , m_x(0)
    , m_y(0)
    , m_xy_x(0)
    , m_xy_y(0)
{
}

void CScaleDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_EDIT_SCALE, m_scale);
    DDV_MinMaxDouble(pDX, m_scale, 0.001, 100);
    DDX_Text(pDX, IDC_EDIT_X, m_x);
	DDV_MinMaxUInt(pDX, m_x, 10, 200000);
    DDX_Text(pDX, IDC_EDIT_Y, m_y);
	DDV_MinMaxUInt(pDX, m_y, 10, 200000);
    DDX_Text(pDX, IDC_EDIT_XY_X, m_xy_x);
	DDV_MinMaxUInt(pDX, m_xy_x, 10, 200000);
    DDX_Text(pDX, IDC_EDIT_XY_Y, m_xy_y);
	DDV_MinMaxUInt(pDX, m_xy_y, 10, 200000);
}

BOOL CScaleDlg::OnInitDialog( )
{
    CString buff;
    buff.Format(_T("(%d x %d)"), m_orig_size.cx, m_orig_size.cy);
    GetDlgItem(IDC_STATIC_ORIGINAL_SIZE)->SetWindowText(buff);
    m_selected = AfxGetApp()->GetProfileInt(REG_SECTION_SETTINGS, _T("SCALING_OPTION"), 0);
    m_scale = AfxGetApp()->GetProfileInt(REG_SECTION_SETTINGS, _T("SCALING_SCALE"), 1000)/1000.;
    m_x = AfxGetApp()->GetProfileInt(REG_SECTION_SETTINGS, _T("SCALING_X"), m_orig_size.cx);
    m_y = AfxGetApp()->GetProfileInt(REG_SECTION_SETTINGS, _T("SCALING_Y"), m_orig_size.cy);
    m_xy_x = AfxGetApp()->GetProfileInt(REG_SECTION_SETTINGS, _T("SCALING_XY_X"), m_orig_size.cx);
    m_xy_y = AfxGetApp()->GetProfileInt(REG_SECTION_SETTINGS, _T("SCALING_XY_Y"), m_orig_size.cy);
    ((CButton*)GetDlgItem(IDC_RADIO_SCALING_NO))->SetCheck(m_selected==0);
    ((CButton*)GetDlgItem(IDC_RADIO_SCALING_SCALE))->SetCheck(m_selected==1);
    ((CButton*)GetDlgItem(IDC_RADIO_SCALING_X))->SetCheck(m_selected==2);
    ((CButton*)GetDlgItem(IDC_RADIO_SCALING_Y))->SetCheck(m_selected==3);
    ((CButton*)GetDlgItem(IDC_RADIO_SCALING_XY))->SetCheck(m_selected==4);
    GetDlgItem(IDC_EDIT_SCALE)->EnableWindow(m_selected==1);
    GetDlgItem(IDC_EDIT_X)->EnableWindow(m_selected==2);
    GetDlgItem(IDC_EDIT_Y)->EnableWindow(m_selected==3);
    GetDlgItem(IDC_EDIT_XY_X)->EnableWindow(m_selected==4);
    GetDlgItem(IDC_EDIT_XY_Y)->EnableWindow(m_selected==4);
	BOOL a = CDialog::OnInitDialog();
	return a;
}

void CScaleDlg::OnBnClicked()
{
    if (((CButton*)GetDlgItem(IDC_RADIO_SCALING_NO))->GetCheck())
        m_selected = 0;
    else if (((CButton*)GetDlgItem(IDC_RADIO_SCALING_SCALE))->GetCheck())
        m_selected = 1;
    else if (((CButton*)GetDlgItem(IDC_RADIO_SCALING_X))->GetCheck())
        m_selected = 2;
    else if (((CButton*)GetDlgItem(IDC_RADIO_SCALING_Y))->GetCheck())
        m_selected = 3;
    else if (((CButton*)GetDlgItem(IDC_RADIO_SCALING_XY))->GetCheck())
        m_selected = 4;
    else
        _ASSERT(0);
    GetDlgItem(IDC_EDIT_SCALE)->EnableWindow(m_selected==1);
    GetDlgItem(IDC_EDIT_X)->EnableWindow(m_selected==2);
    GetDlgItem(IDC_EDIT_Y)->EnableWindow(m_selected==3);
    GetDlgItem(IDC_EDIT_XY_X)->EnableWindow(m_selected==4);
    GetDlgItem(IDC_EDIT_XY_Y)->EnableWindow(m_selected==4);
}

/** The dialog box asking about how multiple pages shall be exported to PDF*/
class CMultipageDlg : public CDialog
{
public:
    CMultipageDlg();

    // Dialog Data
    enum { IDD = IDD_MULTI_PAGE };

protected:
    //virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();
    /** When one of the radio buttons is clicked to change the type of multipage export selected*/
    afx_msg void OnBnClicked();

    // Implementation
protected:
    DECLARE_MESSAGE_MAP()
public:
    int m_selected; ///<The type of multipage export selected.
};

BEGIN_MESSAGE_MAP(CMultipageDlg, CDialog)
    ON_BN_CLICKED(IDC_RADIO_MP1, &CMultipageDlg::OnBnClicked)
    ON_BN_CLICKED(IDC_RADIO_MP2, &CMultipageDlg::OnBnClicked)
    ON_BN_CLICKED(IDC_RADIO_MP3, &CMultipageDlg::OnBnClicked)
END_MESSAGE_MAP()

CMultipageDlg::CMultipageDlg() : CDialog(CMultipageDlg::IDD)
, m_selected(2)
{
}


BOOL CMultipageDlg::OnInitDialog()
{
    auto pConf = GetConf();
    if (!pConf) return false;
    CString opt2 = _T("Export the chart to a single file, where each page is an ");
    opt2 += AsUnicode(ConvertPageSizeVerbose(pConf->m_pageSize));
    opt2 += _T(" page. (Use Print Setup to change; and Print Preview to set margins, scaling and alignment.)");
    ((CButton*)GetDlgItem(IDC_RADIO_MP2))->SetWindowText(opt2);
    ((CButton*)GetDlgItem(IDC_RADIO_MP1))->SetCheck(m_selected==1);
    ((CButton*)GetDlgItem(IDC_RADIO_MP2))->SetCheck(m_selected==2);
    ((CButton*)GetDlgItem(IDC_RADIO_MP3))->SetCheck(m_selected==3);
    BOOL a = CDialog::OnInitDialog();
    return a;
}

void CMultipageDlg::OnBnClicked()
{
    if (((CButton*)GetDlgItem(IDC_RADIO_MP1))->GetCheck())
        m_selected = 1;
    else if (((CButton*)GetDlgItem(IDC_RADIO_MP2))->GetCheck())
        m_selected = 2;
    else if (((CButton*)GetDlgItem(IDC_RADIO_MP3))->GetCheck())
        m_selected = 3;
    else
        _ASSERT(0);
}


////////////////////

////////////////////



AnimationElement::AnimationElement(Element *a, ElementType et, int delay,
                       int appear, int disappear) :
    arc(a), what(et), fade_value(0), fade_delay(delay),
    appe_time(appear), disa_time(disappear), target_fade(1), keep(false)
{
    _ASSERT(et==CONTROL || et==TRACKRECT);
    _ASSERT(arc);
    if (appe_time<0) appe_time = default_appear_time[what];
    if (disa_time<0) disa_time = default_disapp_time[what];
}

AnimationElement::AnimationElement(ElementType et, int delay,
                       int appear, int disappear) :
    arc(nullptr), what(et), fade_value(0), fade_delay(delay),
    appe_time(appear), disa_time(disappear), target_fade(1), keep(false)
{
    _ASSERT(et==FALLBACK_IMAGE || et==COMPILING_GREY);
    if (appe_time<0) appe_time = default_appear_time[what];
    if (disa_time<0) disa_time = default_disapp_time[what];
}

bool AnimationElement::InvertKeep()
{
    keep = !keep;
    target_fade = keep ? 1. : TRACK_MODE_HOOVER_FADE_PERCENT/100.;
    return keep;
}


const unsigned AnimationElement::default_appear_time[MAX_TYPE] = {100, 300, 1, 100};
const unsigned AnimationElement::default_disapp_time[MAX_TYPE] = {100, 300, 500, 100};


// CMscGenDoc

IMPLEMENT_DYNCREATE(CMscGenDoc, COleServerDocEx)

BEGIN_MESSAGE_MAP(CMscGenDoc, COleServerDocEx)
    ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
    ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_EDIT_FIND, OnEditFind)
	ON_COMMAND(ID_EDIT_REPLACE, OnEditReplace)
	ON_COMMAND(ID_EDIT_REPEAT, OnEditRepeat)
    ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_COMMAND(ID_EDIT_COPYENTIRECHART, OnEditCopyEntireChart)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPYENTIRECHART, OnUpdateFileExport)
    ON_COMMAND_RANGE(ID_COPY_PAGE1, ID_COPY_PAGE99, OnCopyPage)
	ON_COMMAND(ID_EDIT_PASETENTIRECHART, OnEditPasteEntireChart)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASETENTIRECHART, OnUpdateEditPasteEntireChart)
    ON_COMMAND(ID_EDIT_UPDATE, OnEditUpdate)
    ON_UPDATE_COMMAND_UI(ID_EDIT_UPDATE, OnUdpateEditUpdate)
    ON_COMMAND(ID_FILE_EXPORT, OnFileExport)
    ON_COMMAND(ID_BUTTON_PREVIEW_EXPORT, OnPreviewExport)
    ON_COMMAND(ID_VIEW_NEXTERROR, OnViewNexterror)
	ON_COMMAND(ID_VIEW_PREVERROR, OnViewPreverror)
    ON_COMMAND(ID_VIEW_NEXTERROR_SMALL, OnViewNexterrorSmall)
    ON_COMMAND(ID_VIEW_PREVERROR_SMALL, OnViewPreverrorSmall)
    ON_COMMAND(ID_VIEW_ZOOMNORMALIZE, OnViewZoomnormalize)
    ON_COMMAND(ID_VIEW_ZOOM100, OnView100Percent)
	ON_COMMAND(ID_VIEW_FITTOWIDTH, OnViewFittowidth)
    ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOMNORMALIZE, OnUpdateViewZoom)
    ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM100, OnUpdateViewZoom)
    ON_UPDATE_COMMAND_UI(ID_VIEW_FITTOWIDTH, OnUpdateViewZoom)
    ON_COMMAND(ID_ZOOMMODE_KEEPINOVERVIEW, OnZoommodeKeepinoverview)
    ON_COMMAND(ID_ZOOMMODE_KEEP100, OnZoommodeKeep100Percent)
	ON_COMMAND(ID_ZOOMMODE_KEEPFITTINGTOWIDTH, OnZoommodeKeepfittingtowidth)
	ON_UPDATE_COMMAND_UI(ID_ZOOMMODE_KEEPINOVERVIEW, OnUpdateZoommodeKeepinoverview)
    ON_UPDATE_COMMAND_UI(ID_ZOOMMODE_KEEP100, OnZoommodeKeep100Percent)
	ON_UPDATE_COMMAND_UI(ID_ZOOMMODE_KEEPFITTINGTOWIDTH, OnUpdateZoommodeKeepfittingtowidth)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_EXPORT, OnUpdateFileExport)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCutCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCutCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_FIND, OnUpdateEditCutCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REPLACE, OnUpdateEditCutCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REPEAT, OnUpdateEditCutCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECT_ALL, OnUpdateEditCutCopy)
	ON_COMMAND(ID_BUTTON_EDITTEXT, OnButtonEdittext)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_EDITTEXT, OnUpdateButtonEdittext)
	ON_COMMAND(ID_BUTTON_TRACK, OnButtonTrack)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_TRACK, OnUpdateButtonTrack)
	ON_COMMAND(ID_INDICATOR_TRK, OnButtonTrack)
    ON_COMMAND(ID_TRACK_HERE, &OnTrackHere)
    ON_UPDATE_COMMAND_UI(ID_TRACK_HERE, &OnUpdateTrackHere)
    ON_COMMAND(ID_BUTTON_COLLAPSE_ALL, &OnButtonCollapseAll)
    ON_COMMAND(ID_BUTTON_EXPAND_ALL, &OnButtonExpandAll)
END_MESSAGE_MAP()

CLIPFORMAT NEAR CMscGenDoc::m_cfPrivate = 0;

// CMscGenDoc construction/destruction

CMscGenDoc::CMscGenDoc() : m_ExternalEditor(this)
{
    // Use OLE compound files, may not be needed for new environment
    EnableCompoundFile();

    auto pConf = GetConf();
    if (!pConf) return;

    // one-time construction code here
    m_cfPrivate = (CLIPFORMAT)::RegisterClipboardFormat(_T("Msc-Generator Signalling Chart"));
    m_ZoomMode = (EZoomMode)GetRegistryInt(REG_KEY_DEFAULTZOOMMODE, 0);
    m_zoom = int(GetRegistryInt(REG_KEY_LAST_ZOOM, 100)*pConf->GetCurrentDPIScalingFactor());
    m_minZoom = 10;
    m_maxZoom = 10000;
    HDC hdc = GetDC(nullptr);
    m_maxAutoZoom = int(150*pConf->GetCurrentDPIScalingFactor());
    m_bTrackMode = false;
    m_saved_charrange.cpMin = -1;
    m_last_arc = nullptr;
    m_pViewFadingTimer = nullptr;

    m_charts.push_back(CChartData());
    m_itrEditing = m_charts.begin();
    m_itrShown = m_charts.end();
    m_itrSaved = m_itrEditing; //start as unmodified
    m_bAttemptingToClose = false;
    m_unicode = false;
    serialize_doc_overhead = 0;
    m_page_serialized_in = -1; //no page data available from serialize
    m_highlight_fallback_images = false;
    m_ChartShown.SetFallbackResolution(pConf->m_uFallbackResolution);
    m_ChartShown.SetPageBreaks(pConf->m_bPageBreaks);
    m_ChartShown.CompileIfNeeded();
    m_pCompilingThread = nullptr;
    m_Progress = 0;
    m_ModTimeOnDiskValid = false;
}

/** Called by the framework to get the COleServerItem associated with the document.
 * It is only called when necessary.*/
COleServerItem* CMscGenDoc::OnGetEmbeddedItem()
{
	CMscGenSrvrItem* pItem = new CMscGenSrvrItem(this, 0);  //Do not force any page
	ASSERT_VALID(pItem);
	return pItem;
}

/** Called by the framework to get the COleServerItem associated with a link to the document.
* It is only called when necessary.*/
COleServerItem* CMscGenDoc::OnGetLinkedItem(LPCTSTR lpszItemName)
{
    // look in current list first
	COleServerItem* pItem = COleServerDoc::OnGetLinkedItem(lpszItemName);
	if (pItem != nullptr)
		return pItem;

    const unsigned page = std::max(0, _ttoi(lpszItemName)); //0 if invalid number - we convert to unsigned(0) if negative
    if (page > m_ChartShown.GetPages())
        return nullptr;

	pItem = new CMscGenSrvrItem(this, page);
	return pItem;
}

/** Called by the framework to get the CDocObjectServer associated with the document.*/
CDocObjectServer *CMscGenDoc::GetDocObjectServer(LPOLEDOCUMENTSITE pDocSite)
{
	return new CDocObjectServer(this, pDocSite);
}

CMscGenDoc::~CMscGenDoc()
{
#ifndef SHARED_HANDLERS
    //save current zoom factor
    theApp.WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_ZOOM,
        int(m_zoom/theApp.m_config.GetCurrentDPIScalingFactor()));
#endif
}

/** Copy current chart text to internal editor*/
void CMscGenDoc::UpdateInternalEditor()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->m_ctrlEditor.UpdateText(m_itrEditing->GetText(),
                                                     m_itrEditing->m_sel, m_itrEditing->m_scroll);
#endif
}

bool CMscGenDoc::IsInFullScreenMode() const
{
#ifdef SHARED_HANDLERS
    return true; //as if - we do not edit in preview or thumbnail mode
#else
    return theApp.m_bFullScreenViewMode;
#endif
}



/** CMscGenDoc serialization */
void CMscGenDoc::Serialize(CArchive& ar)
{
    unsigned page = m_ChartShown.GetPage();
    SerializePage(ar, page);
    if (!ar.IsStoring())
        m_page_serialized_in = page;
    m_uSavedPage = page;
}

/** A helper storing url and file info pairs.*/
struct stringpair
{
    string url, info;
    stringpair(const string &a, const string &b) : url(a), info(b) {}
    bool operator < (const stringpair &o) const { return url==o.url ? info < o.info : url < o.url; }
};

/** Magic string - if file format starts with this string, we have file version later than 2.3.4, but earlier than 5.0*/
#define NEW_VERSION_STRING "@@@Msc-generator later than 2.3.4"
/** Magic string - if file format starts with this string, we have file version later than 5.0*/
#define NEW_VERSION_STRING_CHART "@@@Chart-generator later than 5.0"

/** Serializes out a specific page (whole text marked with that page) or serializes in
 * an object (whole text of it) and learns if a page was selected when saved.
 * All strings we save (including chart text) are in UTF-8 (for backwards comp)
 * This function is separate from Serialize(), because it is directly called by
 * CMscGenSrvrItem::Serialize(), which requires to store a specific page for
 * linked objects.
 * @param ar The archive to work on.
 * @param page On reading we return the page number here (0 if all pages or no page indication);
 *             on writing this tells, which page to write into the object.*/
void CMscGenDoc::SerializePage(CArchive& ar, unsigned &page)
{
    auto pConf = GetConf();
    if (!pConf) return;
	if (ar.IsStoring()) {
		ar << CStringA(NEW_VERSION_STRING_CHART); //if format starts with this string, we have file version afterwards
		ar << unsigned(7); //file format version (continue counting after intriducing NEW_VERSION_STRING_CHART)
        ar << m_ChartShown.GetText();
        ar << page;
        ar << unsigned(LIBMSCGEN_MAJOR);
        ar << unsigned(LIBMSCGEN_MINOR);
        ar << unsigned(LIBMSCGEN_SUPERMINOR);
        ar << CStringA(pConf->m_lang->GetName().c_str()); //language type
        ar << m_ChartShown.GetDesign();
        ar << m_ChartShown.GetLayout(); //layout (used only if graph)
        ar << CStringA(m_ChartShown.m_GUIState.c_str());  //Collapse/expand state
        ar << pConf->m_uFallbackResolution;
        ar << pConf->m_bPageBreaks;
        m_bSavedPageBreaks = pConf->m_bPageBreaks;
        //List shapes used from the design libraries
        if (m_ChartShown.GetUsedShapes().size()) {
            std::multimap<stringpair, string> shapes;
            for (unsigned u : m_ChartShown.GetUsedShapes())
                //Shapes above shapenum are not coming from the design lib,
                //but from the chart file we save now - do not include them in the
                //used shape list
                if (u < pConf->m_lang->shapes.ShapeNum())
                    shapes.emplace(stringpair(pConf->m_lang->shapes[u].GetURL(), pConf->m_lang->shapes[u].GetURL()),
                                   pConf->m_lang->shapes[u].name);
            for (auto i = shapes.begin(); i!=shapes.end(); /*nope*/) {
                size_t count = shapes.count(i->first);
                _ASSERT(count);
                ar << count;
                ar << CStringA(i->first.url.c_str());
                ar << CStringA(i->first.info.c_str());
                for (unsigned u = 0; u<count; u++, i++)
                    ar << CStringA(i->second.c_str());
            }
        }
        ar << 0; //terminating shapes
    } else {
        //we are reading in here
        CChartData chart;
        CStringA first_string;
        CStringA language = "signalling"; //default for old file formats
        ar >> first_string;
        if (first_string == NEW_VERSION_STRING) {
            SerializeInFormat1(ar, chart, page);
        } else if (first_string!=NEW_VERSION_STRING_CHART) {
            SerializeInFormat0(ar, chart, first_string);
            page = 0; // old documents saved all chart, not specific pages
        } else {
            //File format after v5.0
            CStringA text;
            CStringA design;
            CStringA layout;
            CStringA guistate;
            unsigned file_version , a, b, c;
            //read params
            ar >> file_version;
            _ASSERT(file_version == 7);
            ar >> text;
            ar >> page;
            ar >> a;
            ar >> b;
            ar >> c;
            ar >> language;
            ar >> design;
            ar >> layout;
            ar >> guistate;  //Collapse/expand state
            ar >> pConf->m_uFallbackResolution;
            ar >> pConf->m_bPageBreaks;
            //check that all shapes used by the chart are part of our library
            SerializeInShapes(ar, true);
            //sanitize text
            EnsureCRLF(text);
            const int tabsize = GetRegistryInt(REG_KEY_TABSIZE, 4);
            ReplaceTAB(text, tabsize);
            //Now fill in 'chart'
            chart.Set(text);
            chart.SetDesign(design);
            chart.SetLayout(layout);
            chart.m_GUIState = guistate;
            chart.SetVersion(a, b, c);
            m_uSavedFallbackResolution = pConf->m_uFallbackResolution;
            m_bSavedPageBreaks = pConf->m_bPageBreaks;
        }
        /* We have read in the chart. Now set application settings. */
        //Switch to the wanted language
        if (pConf->m_languages.GetLanguage(std::string(language))==nullptr)
            //check if the language is perhaps that of another extension
            language = pConf->m_languages.LanguageForExtension(std::string(language)).c_str();
#ifndef SHARED_HANDLERS
        //language may be empty here: give a msg
        if (!pConf->SetLanguage(language)) {
            CString message = _T("The language of this chart ('")+AsUnicode(language);
            message += _T("') is not available on this system. Using '");
            message += AsUnicode(pConf->m_lang->GetName()) +_T("' language instead. ");
            message += _T("This will likely not work, I am sorry.");
            MessageBox(nullptr, message, _T("Msc-generator error"), MB_OK);
        }
        //Set fallback resolution
        CMainFrame *pMainWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
        if (pMainWnd) {
            CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
            pMainWnd->m_wndRibbonBar.GetElementsByID(ID_EMBEDDEDOPTIONS_FALLBACK_RES, arButtons);
            _ASSERT(arButtons.GetSize()==1);
            CMFCRibbonSlider *s = dynamic_cast<CMFCRibbonSlider *>(arButtons[0]);
            if (s) s->SetPos(pConf->m_uFallbackResolution);
        }
#endif
        //Store the chart
        InsertNewChart(chart);
	} /* not IsStoring */
}

/** Read in a shape number list and check we have all of them in our libs.
 * @param ar An archive we read from, which is positioned to the start of the shapes.
 * @param mess If true, we alert the user via a MessageBox(). */
void CMscGenDoc::SerializeInShapes(CArchive & ar, bool mess)
{
    auto pConf = GetConf();
    if (!pConf) return;
    unsigned count;
    ar >> count;
    CString message;
    bool unknown = false;
    unsigned enumerator = 1;
    while (count) {
        CStringA url, info;
        ar >> url;
        ar >> info;
        bool added = false;
        while (count--) {
            CStringA name;
            ar >> name;
            if (!added && pConf->m_lang->shapes.GetShapeNo(string(name))<0) {
                if (url.GetLength()+info.GetLength()>0) {
                    message.AppendFormat(_T("%u. "), enumerator++);
                    if (info.GetLength()) {
                        message.Append(AsUnicode(info));
                        if (url.GetLength()) {
                            message.Append(_T(" ("));
                            message.Append(AsUnicode(url));
                            message.Append(_T(")"));
                        }
                    } else //only url
                        message.Append(AsUnicode(url));
                    message.Append(_T("\n"));
                } else
                    unknown = true;
                added = true;
            }
        }
        ar >> count;
    }
    if (message.GetLength()) {
        message.Insert(0, _T("The embedded chart contains shapes from the following shape files not present on this machine.\n"));
        if (unknown)
            message.Append(_T("Plus unknown shape files.\n"));
        message.Append(_T("These shapes will be missing."));
    } else if (unknown)
        message = _T("The embedded chart contains shapes not present on this machine.\nThese shapes will be missing.");
    if (message.GetLength() && mess)
        MessageBox(nullptr, message, _T("Msc-generator error"), MB_OK);
}

/** Serialize in the very old format, from before version 2.3.4. */
void CMscGenDoc::SerializeInFormat0(CArchive & ar, CChartData & chart, const CStringA &design)
{
    //Old file format, before 2.3.4
    unsigned read_page; //read_page will be ignored
    ar >> read_page;
    unsigned alloc = 16384;
    unsigned length = 0;
    char *buff = (char*)malloc(alloc);
    while (1) {
        length += ar.Read(buff+length, alloc-1-length);
        if (length == alloc-1) {
            char * const buff2 = (char*)realloc(buff, alloc += 16384);
            if (buff2) buff = buff2;
            else break; //could not allocate more space. Work with truncated file.
        } else break;
    }
    buff[length] = 0;
    CStringA text = buff;
    free(buff);
    EnsureCRLF(text);
    const int tabsize = GetRegistryInt(REG_KEY_TABSIZE, 4);
    ReplaceTAB(text, tabsize);
    chart.Set(text);
    chart.SetDesign(design);
}

/** Serialize in the old format, from between versions 2.3.4 and 4.6.3. */
void CMscGenDoc::SerializeInFormat1(CArchive & ar, CChartData & chart, unsigned &page)
{
    auto pConf = GetConf();
    if (!pConf) return;
    //New file format v2.3.4-v5.0
    unsigned file_version;
    CStringA design, text;
    ar >> file_version;
    _ASSERT(file_version<7);
    if (file_version ==0) {//since v2.3.4
        char *buff;
        unsigned length;
        ar >> design;
        ar >> page;
        ar >> length;
        buff = (char*)malloc(length+1);
        ar.Read(buff, length);
        buff[length] = 0;
        text = buff;
        free(buff);
    } else { //later versions use this
        ar >> design;
        ar >> page;
        ar >> text;
    }
    EnsureCRLF(text);
    const int tabsize = GetRegistryInt(REG_KEY_TABSIZE, 4);
    ReplaceTAB(text, tabsize);
    chart.Set(text);
    chart.SetDesign(design);
    unsigned force_entity_size, force_arc_size;
    unsigned a = 0, b = 0, c = 0;
    if (file_version >= 4) { //since 3.4.3
        ar >> a; //keep "forced_page" intact, "a" is dummy, will be overwritten below
    }
    std::string GUIState;
    if (file_version >= 3) { //since 3.1.3 : read version and force arc collapse
        ar >> a;
        ar >> b;
        ar >> c;
        chart.SetVersion(a, b, c);
        ar >> force_arc_size;
        GUIState << "0\n" << force_arc_size << "\n";
        for (unsigned i = 0; i<force_arc_size; i++) {
            msc::BoxSignature as;
            ar >> as.file_pos.start.file;
            ar >> as.file_pos.start.line;
            ar >> as.file_pos.start.col;
            ar >> as.file_pos.end.file;
            ar >> as.file_pos.end.line;
            ar >> as.file_pos.end.col;
            ar >> a;
            GUIState << msc::EBoxCollapseTypeToChar(msc::EBoxCollapseType(a)) << ' ';
            GUIState << as.Serialize() << '\n';
        }
    }
    if (file_version >= 2) { //since v3.1: read force entity collapse
        std::string GUIState2;
        ar >> force_entity_size;
        GUIState2 << "0\n" << force_entity_size << "\n";
        for (unsigned i = 0; i<force_entity_size; i++) {
            CStringA s; unsigned b2;
            ar >> s;
            ar >> b2;
            GUIState2 << (b2!=0 ? "1 " : "0 ") << s << "\n";
        }
        //prepend
        GUIState = GUIState2 + GUIState;
    }
    pConf->m_uFallbackResolution = 300; //default before 3.5.3
    if (file_version >= 5) { //since 3.5.3
        ar >> pConf->m_uFallbackResolution;
        m_uSavedFallbackResolution = pConf->m_uFallbackResolution;
        ar >> pConf->m_bPageBreaks;
        m_bSavedPageBreaks = pConf->m_bPageBreaks;
    }
    if (file_version >= 6)  // since 3.7.5
        SerializeInShapes(ar, true);
}



/** Remove all contents from us.*/
void CMscGenDoc::DeleteContents()
{
    KillCompilation();
    m_charts.clear();
	m_charts.push_back(CChartData());
	m_itrEditing = m_charts.begin();
	m_itrShown = m_charts.end();
	m_itrSaved = m_charts.end(); //start as modified
	m_ChartShown.Set("");
    m_ChartShown.DeleteGUIState();
    m_ChartShown.CompileIfNeeded();
	COleServerDocEx::DeleteContents();
}

/** Tests if we can be closed. */
BOOL CMscGenDoc::CanCloseFrame(CFrameWnd* pFrame)
{
    KillCompilation();
	m_bAttemptingToClose = true;
	BOOL ret = COleServerDocEx::CanCloseFrame(pFrame);
	m_bAttemptingToClose = false;
	return ret;
}

/** Called by a framework, when a new object is inserted into a document.
 * We ask its language first. */
void CMscGenDoc::OnNewEmbedding(LPSTORAGE lpStorage)
{
#ifndef SHARED_HANDLERS
    if (theApp.AskForLanguage())
#endif
        COleServerDocEx::OnNewEmbedding(lpStorage);
}


/** Called by the framework when establishing a new document.
 * We delete the undo buffer and fill in with the default chart text and initiate
 * parallel compilation. If external editor is running, we kill and restart it.
 * If internal editor is running, we update its text (triggering color syntax
 * highlighting if enabled). This is always called when we are started to handle
 * OLE. If a new object needs to be created, it is called twice!! So
 * we ask the user about the file type in OnNewEmbedding() instead.*/
BOOL CMscGenDoc::OnNewDocument()
{
    m_ModTimeOnDiskValid = false;
	bool restartEditor = m_ExternalEditor.IsRunning();
	if (restartEditor)
		m_ExternalEditor.Stop(STOPEDITOR_WAIT);
	if (!COleServerDocEx::OnNewDocument())
		return FALSE;

	// (SDI documents will reuse this document)
    auto pConf = GetConf();
    if (!pConf) return false;
    CChartData data;
    data.Set(pConf->m_lang->default_text.c_str());
	m_charts.clear();
	m_charts.push_back(data);
	m_itrEditing = m_charts.begin();
	m_itrSaved = m_itrEditing; //start as unmodified
    SetModifiedFlag(FALSE);

    m_uSavedFallbackResolution = pConf->m_uFallbackResolution;
    m_bSavedPageBreaks = pConf->m_bPageBreaks;
    m_uSavedPage = 0;

    UpdateInternalEditor();
    CompileEditingChart(true, false, false);
	if (restartEditor)
		m_ExternalEditor.Start(_T("Untitled"));
	return TRUE;
}

/** Called by the framework when we open a chart.
 * We delete undo buffer, compile and kill/restart external editor (if needed).
 * If internal editor is running, we update its text (triggering color syntax
 * highlighting if enabled).
 * @param [in] lpszPathName If the chart is opened from a file, this is the file name.
 *                          If the chart is opened from a container document, it is nullptr.
 * @returns true on success.*/
BOOL CMscGenDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	ASSERT_VALID(this);
	ASSERT(lpszPathName == nullptr || AfxIsValidString(lpszPathName));

	bool restartEditor = m_ExternalEditor.IsRunning();
	if (restartEditor)
		m_ExternalEditor.Stop(STOPEDITOR_WAIT);

	if (lpszPathName == nullptr) {
		//this one will use serialize which sets m_itrCurrent to loaded entry
		if (!COleServerDocEx::OnOpenDocument(lpszPathName))
			return FALSE;
        //Copy the saved display parameters to current chart showing from where they
        //will be copied to the chart under compilation
        m_ChartShown.SetPage(m_uSavedPage);
        m_ChartShown.SetFallbackResolution(m_uSavedFallbackResolution);
        m_ChartShown.SetPageBreaks(m_bSavedPageBreaks);
        m_ModTimeOnDiskValid = false;
	} else {
		// always register the document before opening it
		Revoke();
		if (!RegisterIfServerAttached(lpszPathName, FALSE))
		{
			// always output a trace (it is just an FYI -- not generally fatal)
			TRACE(traceOle, 0, _T("Warning: Unable to register moniker '%s' as running\n"), lpszPathName);
		}

		USES_CONVERSION;

		if (IsModified())
			TRACE(traceOle, 0, "Warning: OnOpenDocument replaces an unsaved document.\n");

        //Determine language
        auto pConf = GetConf();
        if (!pConf) return FALSE;
        TCHAR *dot = PathFindExtension(lpszPathName)+1; //pointer to the extension dot in 'name'
        CChartData data;
        std::string lang;
        if (CaseInsensitiveEqual((const char*)AsUTF8(dot), "png")) {
            FILE *in;
            MscError Error;
            std::string input;
            std::string gui_state;
            if (errno_t err = _wfopen_s(&in, lpszPathName, L"rb")) {
                Error.FatalError(FileLineCol(), "Could not open file.");
            } else {
                EmbedChartData data = pngutil::Select(in, pConf->m_languages, Error);
                lang = std::move(data.chart_type);
                input = std::move(data.chart_text);
                gui_state = std::move(data.gui_state);
                m_page_serialized_in = data.page; //This will be picked up in CompileEditingChart() below
                fclose(in);
            }
            if (Error.GetErrorNum(true, false))
                MessageBox(nullptr, AsUnicode(Error.Print(true, false)), _T("Msc-generator"), MB_OK|MB_ICONEXCLAMATION);
            if (lang.length()) {
                m_unicode = false; //embedded is always UTF-8
                data.Set(input.c_str());
                data.m_GUIState = std::move(gui_state);
            } else {
                m_ModTimeOnDiskValid = false;
                return FALSE;
            }
        } else {
            lang = pConf->m_languages.LanguageForExtension((const char*)AsUTF8(dot));
            if (lang.length()==0) {
                if (m_suggested_lang.length()==0) {
                    MessageBox(nullptr, _T("Could not determine chart language from extension. Using 'signalling'."), _T("Msc-generator"), MB_OK|MB_ICONEXCLAMATION);
                    lang = "signalling";
                } else {
                    lang = m_suggested_lang;
                    m_suggested_lang.clear();
                }
            }
            if (!data.Load(lpszPathName, false, &m_unicode)) {
                m_ModTimeOnDiskValid = false;
                return FALSE;
            }
        }
        //At this point 'data' contains the chart text and 'lang' contains the requested type.

        //OK, store date
        CFileStatus status;
        m_ModTimeOnDiskValid = CFile::GetStatus(lpszPathName, status);
        m_ModTimeOnDisk = status.m_mtime;

		data.SetDesign(m_itrEditing->GetDesign());
        if (lang != pConf->m_lang->GetName())
            pConf->SetLanguage(lang.c_str());
		InsertNewChart(data); //all pages visible, undo blocked
	}
	//Delete all entries before the currently loaded one (no undo)
	//part after (redo) was deleted by Serialize or InsertNewChart
	//Here we set all our m_itr* iterators to valid values afterwards
	m_charts.erase(m_charts.begin(), m_itrEditing);
	m_itrSaved = m_itrEditing;
    m_itrShown = m_charts.end();
    SetModifiedFlag(FALSE);

#ifndef SHARED_HANDLERS
    //Copy text to the internal editor
    UpdateInternalEditor();
    //Hide Autosave and Recovery panel (if visible)
    if (CMainFrame* pMain = dynamic_cast<CMainFrame*>(AfxGetMainWnd()))
        pMain->ActivateHomeCategory();
	// if the app was started only to print or to display an embedded object, don't set user control
	//Copied from COleLinkingDoc
    const bool user = theApp.m_pCmdInfo == nullptr ||
		(theApp.m_pCmdInfo->m_nShellCommand != CCommandLineInfo::FileDDE &&
		 theApp.m_pCmdInfo->m_nShellCommand != CCommandLineInfo::FilePrint);
	if (user)
		AfxOleSetUserCtrl(TRUE);
#else
#endif
    m_strPathName = lpszPathName; //set it so that file name is already available for compilation (to have include files found correctly)
    CompileEditingChart(true, false, false);
	if (restartEditor)
		m_ExternalEditor.Start(lpszPathName);
	return TRUE;
}

/** Called by the framework when we need to save the document.
 * If the chart is embedded, we force a compile (in a blocking, synchronous way part of this
 * thread). Note that this can be an autosave to a file, a SaveAs, a SaveCopyAs or a serializing to
 * an embedded object.
 * @param [in] lpszPathName If the chart is to be saved to a file, this is the file name.
 *                          If the chart is to be embedded to a container document, it is nullptr.
 * @returns true on success.*/
BOOL CMscGenDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
    bool restartEditor = false;
    m_ModTimeOnDiskValid = false; //disable "changed on disk" warnings
	if (lpszPathName==nullptr) {
        CompileEditingChart(false, true, false);
        if (!COleServerDocEx::OnSaveDocument(lpszPathName))
            return FALSE;
        //for embedded objects mark this chart as saved
        //(for saving to a file, we cannot determine if this is an autosave or not, so we
        //do this in DoSave() instead.
        m_itrSaved = m_itrEditing;
    } else if (CaseInsensitiveEqual((const char*)AsUTF8(CString(PathFindExtension(lpszPathName))), ".png")) {
        //OK, we shall be written to a PNG file.
        CompileEditingChart(false, true, false); //Compile blocking
        if (m_ChartShown.GetErrorNum(false, false))
            if (IDCANCEL == AfxMessageBox(_T("The chart had errors, do you want to save its image nevertheless?"),
                                          MB_ICONQUESTION | MB_OKCANCEL))
                return FALSE;
        auto pConf = GetConf();
        if (!pConf) return FALSE;
        const CStringA err = m_ChartShown.DrawToFile(lpszPathName, false, false, true);
        if (err.GetLength()) {
            AfxMessageBox(AsUnicode(err));
            return FALSE;
        }
    } else {
        if (!m_itrEditing->Save(lpszPathName, m_unicode))
            return FALSE;
    }
    return TRUE;
}

/** Called by the framework if the document needs closing.
 * We kill the external editor and abort any pending compilations.*/
void CMscGenDoc::OnCloseDocument()
{
	m_ExternalEditor.Stop(STOPEDITOR_FORCE);
    KillCompilation();
	COleServerDocEx::OnCloseDocument();
}


/**  Save the document data to a file.
 *  @param lpszPathName The path name where to save document file
 *                      if lpszPathName is nullptr then the user will be prompted (SaveAs).
 *                      note: lpszPathName can be different than 'm_strPathName'
 *  @param bReplace If TRUE will change file name if successful (SaveAs),
 *                  if FALSE will not change path name (SaveCopyAs)
 * This takes a lot from CDocument::DoSave(), we just calculate suggested
 * name for SaveAs differently and assemble a different extension filter list
 * than the one coming from the document template.
 * Restart the external editor if the file name has changed, but we are not saving a copy.*/
BOOL CMscGenDoc::DoSave(LPCTSTR lpszPathName, BOOL bReplace)
{
    CString newName = lpszPathName;
    auto pConf = GetConf();
    if (!pConf) return false;
    if (newName.IsEmpty()) {
        CDocTemplate* pTemplate = GetDocTemplate();
        ASSERT(pTemplate != nullptr);

        newName = m_strPathName;
        if (bReplace && newName.IsEmpty()) {
            newName = m_strTitle;
            // check for dubious filename
            int iBad = newName.FindOneOf(_T(":/\\*"));
            if (iBad != -1)
                newName.ReleaseBuffer(iBad);
            if (newName.GetLength() > _MAX_FNAME)
                newName.ReleaseBuffer(_MAX_FNAME);

            if (AfxGetApp() && AfxGetApp()->GetDataRecoveryHandler()) {
                // remove "[recovered]" from the title if it exists
                CString strNormalTitle = AfxGetApp()->GetDataRecoveryHandler()->GetNormalDocumentTitle(this);
                if (!strNormalTitle.IsEmpty())
                    newName = strNormalTitle;
            }
            //append default extension
            newName += _T(".");
            newName += AsUnicode(pConf->m_lang->GetName());
        }

        //OK, now we have a suggestion for the name, open the dialog
        CFileDialog dlgFile(FALSE, nullptr, nullptr, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, nullptr, nullptr, 0);
        CString title;
        ENSURE(title.LoadString(bReplace ? AFX_IDS_SAVEFILE : AFX_IDS_SAVEFILECOPY));
        //Assemble filter list
        CString strDefault = AsUnicode(pConf->m_lang->GetName());
        CString strFilter = AsUnicode(pConf->m_lang->description);
        strFilter += (TCHAR)'\0';
        for (auto &e : pConf->m_lang->extensions) {
            if (strFilter[strFilter.GetLength()-1]) //not ending in nullptr
                strFilter += _T(";");
            strFilter += _T("*.");
            strFilter += AsUnicode(e);
        }
        strFilter += (TCHAR)'\0';
        dlgFile.m_ofn.nMaxCustFilter = 1;
        dlgFile.m_ofn.nFilterIndex = 0;

        // append the "*.png" filter
        strFilter += _T("PNG embedded chart");
        strFilter += (TCHAR)'\0';   // next string please
        strFilter += _T("*.png");
        strFilter += (TCHAR)'\0';   // last string
        dlgFile.m_ofn.nMaxCustFilter++;
        // append the "*.*" all files filter
        CString allFilter;
        VERIFY(allFilter.LoadString(AFX_IDS_ALLFILTER));
        strFilter += allFilter;
        strFilter += (TCHAR)'\0';   // next string please
        strFilter += _T("*.*");
        strFilter += (TCHAR)'\0';   // last string
        dlgFile.m_ofn.nMaxCustFilter++;

        dlgFile.m_ofn.lpstrFilter = strFilter;
        dlgFile.m_ofn.lpstrTitle = title;
        dlgFile.m_ofn.lpstrFile = newName.GetBuffer(_MAX_PATH);

        if (dlgFile.DoModal() != IDOK)
            return FALSE;       // don't even attempt to save

        newName.ReleaseBuffer(); //terminating null char is added by DoModal(), no need to specify length
        //Check new name and add extension
        TCHAR *dot = PathFindExtension(newName); //pointer to the extension dot in 'newName'
        if (*dot==0) {
            if (dlgFile.m_ofn.nFilterIndex==2) //indexing starts from 1
                newName += _T(".png"); // no extension by used, but png filter is selecte: add png
            else
                newName += _T(".") + AsUnicode(pConf->m_lang->GetName()); // no extension by used: add primary extension
        }
    }

    CWaitCursor wait;

    //Restart external editor only if file name changes
    const bool restartEditor = bReplace && (lpszPathName==nullptr || lpszPathName!=GetPathName()) && m_ExternalEditor.IsRunning() ;
    if (restartEditor)
        m_ExternalEditor.Stop(STOPEDITOR_WAIT);

    if (!OnSaveDocument(newName)) {
        if (lpszPathName == nullptr) {
            // be sure to delete the file
            TRY
            {
                CFile::Remove(newName);
            }
                CATCH_ALL(e)
            {
                TRACE(traceAppMsg, 0, "Warning: failed to delete file after failed SaveAs.\n");
                if (e) e->Delete();
            }
            END_CATCH_ALL
        }
        if (restartEditor)
            m_ExternalEditor.Start(m_strPathName); //restart external editor on old path name
        return FALSE;
    }

    // reset the title and change the document name
    if (bReplace) {
        SetPathName(newName);
        OnDocumentEvent(onAfterSaveDocument);
        m_itrSaved = m_itrEditing; //for saves to a file (as opposed to embedding), we did not do this in OnSaveDocument(), because we did not know bReplace
        SetModifiedFlag(FALSE);
        //Update modification time
        CFileStatus status;
        m_ModTimeOnDiskValid = CFile::GetStatus(newName, status);
        m_ModTimeOnDisk = status.m_mtime;
    }
    if (restartEditor)
        m_ExternalEditor.Start(m_strPathName);

    return TRUE;        // success
}

void CMscGenDoc::OnIdle()
{
    COleServerDocEx::OnIdle();
#ifndef SHARED_HANDLERS
    if (IsEmbedded()) return;
    if (!m_ModTimeOnDiskValid) return;
    if (!IsInternalEditorVisible()) return;
    if (m_ExternalEditor.IsRunning()) return;
    CString filename = GetPathName();
    if (filename.GetLength()==0) return;
    CFileStatus status;
    if (!CFile::GetStatus(filename, status)) return;
    if (m_ModTimeOnDisk >= status.m_mtime) return;
    m_ModTimeOnDisk = status.m_mtime;
    if (IDYES != AfxGetMainWnd()->MessageBox(
        _T("The file in the internal editor has changed. Do you want to reload it?"),
        _T("Msc-generator"), MB_YESNO))
        return;
    //Reload file
    CChartData data;
    TCHAR *dot = PathFindExtension(filename)+1; //pointer to the extension dot in 'name'
    if (CaseInsensitiveEqual(AsUTF8str(dot), "png")) {
        MscError Error;
        FILE* in;
        if (!_wfopen_s(&in, filename, L"rb")) return; //Could not open file on disk, ignore silently.
        EmbedChartData embedded_data = pngutil::Select(in, GetConf()->m_languages, Error);
        fclose(in);
        if (Error.GetErrorNum(true, false))
            MessageBox(nullptr, AsUnicode(Error.Print(true, false)), _T("Msc-generator"), MB_OK|MB_ICONEXCLAMATION);
        if (embedded_data.empty()) return;
        m_unicode = false; //embedded is always UTF-8
        data.Set(embedded_data.chart_text.c_str());
        data.m_GUIState = std::move(embedded_data.gui_state);
    } else
        if (!data.Load(filename, false, &m_unicode)) return;
    data.SetDesign(m_itrEditing->GetDesign());
    InsertNewChart(data);
	//Copy text to the internal editor
    if (GetInternalEditor())
        theApp.m_pWndEditor->m_ctrlEditor.UpdateText(m_itrEditing->GetText(),
            m_itrEditing->m_sel, m_itrEditing->m_scroll);
#endif
}


/** Called by the framework to get the status of the File Export button.
 * We enable it if we have a non-empty chart.*/
void CMscGenDoc::OnUpdateFileExport(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(!m_ChartShown.IsEmpty() || !m_itrEditing->IsEmpty());
}

/** Called by the framework when the export button is pressed in the main window
 * (and not in Print Preview). We pass control to DoExport()*/
void CMscGenDoc::OnFileExport()
{
    DoExport(false);
}

/** Called by the framework when the export button is pressed in Print Preview.
 * We pass control to DoExport()*/
void CMscGenDoc::OnPreviewExport()
{
    DoExport(true);
}

/** Perform an exporting of the current chart.
 * We ask for filename, offer scaling and/or multi-page options and perform the action.
 * @param [in] pdfOnly If true, we offer only PDF format, and assume multi-page PDF output
 *                     (as seen in Print Preview).*/
void CMscGenDoc::DoExport(bool pdfOnly)
{
    CompileEditingChart(false, false, false); //Start a compilation - usually in bkg, till the user selects file
    CString name = GetPathName();
    XY scale(1,1); //scale from chart pixels to points (=user scaling)
    bool bitmap_format = false, pdf_format = false, ppt_format = false;
    do {  //must not call DoModal twice for the same instance of CFileDialog - new scope
        CFileDialog dialog(false);
        dialog.m_pOFN->Flags &= ~OFN_OVERWRITEPROMPT & ~OFN_NOTESTFILECREATE;
        dialog.m_pOFN->lpstrTitle = _T("Export to file");
        if (pdfOnly) {
            dialog.m_pOFN->lpstrFilter = _T("Portable Document Format (*.pdf)\0*.pdf\0");
            dialog.m_pOFN->nFilterIndex = 1;
        } else {
            dialog.m_pOFN->lpstrFilter =
                _T("Portable Network Graphics (*.png)\0*.png\0")
                _T("Windows Bitmap (*.bmp)\0*.bmp\0")
                _T("Enhanced Metafile (*emf)\0*.emf\0")
                _T("Scalable Vector Graphics (*svg)\0*.svg\0")
                _T("Portable Document Format (*.pdf)\0*.pdf\0")
                _T("Encapsulated PostScript (*.eps)\0*.eps\0")
                _T("PowerPoint (*.pptx)\0*.pptx\0");
                dialog.m_pOFN->nFilterIndex = GetRegistryInt(_T("ExportFileType"), 1);
            if (dialog.m_pOFN->nFilterIndex>7 || dialog.m_pOFN->nFilterIndex<1)
                dialog.m_pOFN->nFilterIndex = 1;
        }
        //Remove the extension from the filename if one of the
        //valid extensions for the current language.
        TCHAR *dot = PathFindExtension(name); //pointer to the extension dot in 'name'
        auto pConf = GetConf();
        if (pConf)
            for (auto &ext : pConf->m_lang->extensions)
                if (!_tcscmp(dot, L"."+AsUnicode(ext))) {
                    name.Delete(int(dot-name), 100);
                    break;
                }
        TCHAR buff[1024];
        _tcscpy_s(buff, name);
        dialog.m_pOFN->lpstrFile = buff;
        dialog.m_pOFN->nMaxFile = 1024;
        //dialog.ApplyOFNToShellDialog();
        if (dialog.DoModal() != IDOK)
            return;
#ifndef SHARED_HANDLERS
        if (!pdfOnly)
            theApp.WriteProfileInt(REG_SECTION_SETTINGS, _T("ExportFileType"), dialog.m_pOFN->nFilterIndex);
#endif
        //dialog.UpdateOFNFromShellDialog();
        name = dialog.GetPathName();
        CString ext = PathFindExtension(name);
        //if we do not recognize the extension typed by the user, we add one from the selected type
        if (pdfOnly) {
            if (ext.CompareNoCase(_T(".pdf"))!=0)
                name += _T(".pdf");
        } else if (ext.CompareNoCase(_T(".png"))!=0 && ext.CompareNoCase(_T(".bmp"))!=0 &&
                   ext.CompareNoCase(_T(".emf"))!=0 && ext.CompareNoCase(_T(".svg"))!=0 &&
                   ext.CompareNoCase(_T(".pdf"))!=0 && ext.CompareNoCase(_T(".eps"))!=0 &&
                   ext.CompareNoCase(_T(".pptx"))!=0 && ext.CompareNoCase(_T(".wmf"))!=0) { //undocumented wmf export
            switch(dialog.m_pOFN->nFilterIndex) {
            case 1: name += _T(".png"); break;
            case 2: name += _T(".bmp"); break;
            case 3: name += _T(".emf"); break;
            case 4: name += _T(".svg"); break;
            case 5: name += _T(".pdf"); break;
            case 6: name += _T(".eps"); break;
            case 7: name += _T(".pptx"); break;
            default: _ASSERT(0);
            }
        }
        if (PathFileExists(name))
            if (IDNO == AfxMessageBox(_T("File ") + name + _T(" exists. Do you want to overwrite?"), MB_YESNO))
                continue;
        ext = PathFindExtension(name);
        bitmap_format = ext.CompareNoCase(_T(".png"))==0 || ext.CompareNoCase(_T(".bmp"))==0;
        pdf_format = ext.CompareNoCase(_T(".pdf"))==0;
        ppt_format = ext.CompareNoCase(_T(".pptx"))==0;
        break;
    } while(1);

    WaitForCompilationToEnd();
    if (m_ChartShown.GetErrorNum(false, false))
        if (IDCANCEL == AfxMessageBox(_T("The chart had errors, do you want to export it nevertheless?"),
                                      MB_ICONQUESTION | MB_OKCANCEL))
            return;

    auto pConf = GetConf();
    if (!pConf) return;

    bool ignore_pagebreaks = false;
    XY page_size(0, 0);
    if (pdfOnly) {  //For print preview export, we always choose #option 2 - multi-page PDF output
        page_size = GetPhysicalPageSize(pConf->m_pageSize);
    } else if (pdf_format && m_ChartShown.GetPages()>1) {
        CMultipageDlg multipage;
        multipage.m_selected = 2;
        if (IDCANCEL == multipage.DoModal())
            return;
        switch (multipage.m_selected) {
        case 1:
            ignore_pagebreaks = true;
            break;
        case 2:
            ignore_pagebreaks = false;
            page_size = GetPhysicalPageSize(pConf->m_pageSize);
            break;
        default:
            _ASSERT(0);
            FALLTHROUGH;
        case 3:
            ignore_pagebreaks = false;
        }
    } else if (ppt_format) {
        if (m_ChartShown.GetPages()>1) {
            switch (MessageBox(nullptr, _T("Do you want each chart page printed on a separate slide?\n(Margins and alignment can be set via Print Preview, page size is Widescreen lanscape)."), _T("PPT Export"), MB_YESNOCANCEL|MB_ICONQUESTION)) {
            case IDYES: ignore_pagebreaks = false; break;
            case IDNO: ignore_pagebreaks = true; break;
            default:
            case IDCANCEL: return;
            }
        }
        page_size = GetPhysicalPageSize(DEFAULT_PPT_PAGE_SIZE);
    } else if (bitmap_format) {
        CScaleDlg dlg;
        dlg.m_orig_size = m_ChartShown.GetSize();
        if (m_ChartShown.GetMscWidthAttr()>0) {
            const double sc = m_ChartShown.GetMscWidthAttr()/dlg.m_orig_size.cx;
            dlg.m_orig_size.cx = (LONG)(dlg.m_orig_size.cx*sc);
            dlg.m_orig_size.cy = (LONG)(dlg.m_orig_size.cy*sc);
            scale.x = scale.y = sc;
        }
        if (IDCANCEL == dlg.DoModal())
            return;
        /* scale now is either 1 or the scaling required by the width attr in mscgen compat mode*/
        /* similar, dlg.m_orig_size is either the original page size or the page size scaled
         * to fit the width attr in mscgen compat mode */
        switch (dlg.m_selected) {
        default: _ASSERT(0); FALLTHROUGH; //no break, release edition falls through to no scaling
        case 0: /*use scale as set above or as initialized to 1 */ break;
        case 1: scale.x = scale.y *= dlg.m_scale; break;
        /*dlg.m_orig_size is already scaled by the width attr in mscge compat mode,
         *so we can overwrite 'scale' by a value calculated from it*/
        case 2: scale.x = scale.y = double(dlg.m_x)/dlg.m_orig_size.cx; break;
        case 3: scale.x = scale.y = double(dlg.m_y)/dlg.m_orig_size.cy; break;
        case 4:
            scale.x = double(dlg.m_x)/dlg.m_orig_size.cx;
            scale.y = double(dlg.m_y)/dlg.m_orig_size.cy;
            break;
        }
        AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, _T("SCALING_OPTION"), dlg.m_selected);
        AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, _T("SCALING_SCALE"), int(dlg.m_scale*1000));
        AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, _T("SCALING_X"), dlg.m_x);
        AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, _T("SCALING_Y"), dlg.m_y);
        AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, _T("SCALING_XY_X"), dlg.m_xy_x);
        AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, _T("SCALING_XY_Y"), dlg.m_xy_y);
    } else {
        //PDF, EMF, WMF, PPT
        CSize orig_size = m_ChartShown.GetSize();
        scale.x = scale.y = pConf->CalculateScaleFor(orig_size.cx, orig_size.cy);
    }
    CStringA err =
        m_ChartShown.DrawToFile(name, false, ignore_pagebreaks, false, scale, page_size,
                                pConf->m_printer_usr_margins, pConf->m_iPageAlignment - (pConf->m_iPageAlignment/3)*3,
                                pConf->m_iPageAlignment/3);
    if (err.GetLength())
        AfxMessageBox(AsUnicode(err));
}

/**  Called by the framework to get the state of the Undo button.
 * We enable if the undo buffer has elements prior the current editing chart.*/
void CMscGenDoc::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_itrEditing != m_charts.begin());
}

/** Called by the framework when the user invokes undo.
 * We perfrom the undo, if we can, update text in the internal editor and
 * restart the external editor, if running.*/
void CMscGenDoc::OnEditUndo()
{
	if (m_itrEditing == m_charts.begin()) return;
	bool restartEditor = m_ExternalEditor.IsRunning();
	if (restartEditor)
		m_ExternalEditor.Stop(STOPEDITOR_WAIT);
	//Step back and update the internal editor
	m_itrEditing--;
    UpdateInternalEditor();
    CheckIfChanged();
	if (restartEditor)
		m_ExternalEditor.Start();
}

/**  Called by the framework to get the state of the Redo button.
* We enable if the undo buffer has elements after the current editing chart.*/
void CMscGenDoc::OnUpdateEditRedo(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_itrEditing != --m_charts.end());
}

/** Called by the framework when the user invokes redo.
* We perfrom the redo, if we can, update text in the internal editor and
* restart the external editor, if running.*/
void CMscGenDoc::OnEditRedo()
{
	if (m_itrEditing == --m_charts.end()) return;
	bool restartEditor = m_ExternalEditor.IsRunning();
	if (restartEditor)
		m_ExternalEditor.Stop(STOPEDITOR_WAIT);
	m_itrEditing++;
    UpdateInternalEditor();
	CheckIfChanged();
	if (restartEditor)
		m_ExternalEditor.Start();
}

/** Called by the framework to get the status of the cut and copy buttons.
 * We enable if the internal editor is running.*/
void CMscGenDoc::OnUpdateEditCutCopy(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(IsInternalEditorVisible());
}

/** Called by the framework if the user invoked cut.
 * We pass it on to the internal editor.*/
void CMscGenDoc::OnEditCut()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->m_ctrlEditor.Cut();
#endif
}

/** Called by the framework if the user invoked copy.
 * We pass it on to the internal editor.*/
void CMscGenDoc::OnEditCopy()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->m_ctrlEditor.Copy();
#endif
}

/** Called by the framework to get the status of the paste buttons.
 * We enable if clipboard has text on it.*/
void CMscGenDoc::OnUpdateEditPaste(CCmdUI *pCmdUI)
{
	COleDataObject dataObject;
	dataObject.AttachClipboard();
	pCmdUI->Enable(dataObject.IsDataAvailable(CF_TEXT) && IsInternalEditorVisible());
}

/** Called by the framework if the user invoked paste.
* We pass it on to the internal editor.*/
void CMscGenDoc::OnEditPaste()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor()) {
        COleDataObject dataObject;
        dataObject.AttachClipboard();
        if (dataObject.IsDataAvailable(CF_UNICODETEXT))
            theApp.m_pWndEditor->m_ctrlEditor.PasteSpecial(CF_UNICODETEXT);
        else if (dataObject.IsDataAvailable(CF_TEXT))
            theApp.m_pWndEditor->m_ctrlEditor.PasteSpecial(CF_TEXT);
    }
#endif
}

/** Called by the framework when the user invokes find.
 * We pass it on to CEditorBar::OnEditFindReplace().*/
void CMscGenDoc::OnEditFind()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->OnEditFindReplace(true);
#endif
}

/** Called by the framework when the user invokes repeat find.
* We pass it on to CEditorBar::OnEditRepeat().*/
void CMscGenDoc::OnEditRepeat()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->OnEditRepeat();
#endif
}

/** Called by the framework when the user invokes replace.
 * We pass it on to CEditorBar::OnEditFindReplace().*/
void CMscGenDoc::OnEditReplace()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->OnEditFindReplace(false);
#endif
}


/** Called by the framework if the user invoked Copy Entire chart.
 * We pass it on to CMscGenSrvrItem::CopyToClipboard()*/
void CMscGenDoc::OnEditCopyEntireChart()
{
	//Copy is handled by SrvItem
    CompileEditingChart(false, true, false);

#ifndef SHARED_HANDLERS
    if (m_ChartShown.GetErrorNum(false, false))
        if (IDCANCEL == AfxMessageBox(_T("The chart had errors, do you want to copy it nevertheless?"),
                                      MB_ICONQUESTION | MB_OKCANCEL))
            return;
#endif
    CMscGenSrvrItem *pItem = dynamic_cast<CMscGenSrvrItem*>(COleServerDocEx::GetEmbeddedItem());
    TRY
    {
        pItem->CopyToClipboard(TRUE);
    }
    CATCH_ALL(e)
    {
        AfxMessageBox(_T("Copy to clipboard failed"));
    }
    END_CATCH_ALL
}

/** Called by the framework if the user invoked Copy a page.
 * We pass it on to COleServerItem::CopyToClipboard()*/
void CMscGenDoc::OnCopyPage(UINT id)
{
    const unsigned page = id - ID_COPY_PAGE1 + 1;
    CString buff;
    CompileEditingChart(false, true, false);
    if (m_ChartShown.GetErrorNum(false, false)) {
#ifndef SHARED_HANDLERS
        if (IDCANCEL == AfxMessageBox(_T("The chart had errors, do you want to copy a page of it nevertheless?"),
                                      MB_ICONQUESTION | MB_OKCANCEL))
            return;
#endif
    }
    if (m_ChartShown.GetPages() < page) {
#ifndef SHARED_HANDLERS
        buff.Format(_T("Sorry, I no longer have page #%u, ")
                    _T("the entire chart consists of only %u page(s). ")
                    _T("I did not change the clipboard."),
                    page, m_ChartShown.GetPages());
        AfxMessageBox(buff);
#endif
    } else {
        TRY
        {
            buff.Format(_T("%u"), page);
            //Copy is handled by SrvItem
            COleServerItem *pItem = OnGetLinkedItem(buff);
            pItem->CopyToClipboard(TRUE);
        }
        CATCH_ALL(e)
        {
#ifndef SHARED_HANDLERS
            AfxMessageBox(_T("Copy to clipboard failed"));
#endif
        }
        END_CATCH_ALL
    }
}

/** Called by the framework to get the status of the Paste Entire Chart button.
 * We enable if the clipboard has text or chart data.*/
void CMscGenDoc::OnUpdateEditPasteEntireChart(CCmdUI *pCmdUI)
{
	COleDataObject dataObject;
	dataObject.AttachClipboard();
	pCmdUI->Enable(!IsInFullScreenMode() && (dataObject.IsDataAvailable(m_cfPrivate) ||
						          					 dataObject.IsDataAvailable(CF_TEXT)));
}

/** Paste data from `dataObject`.
 * If data object is a chart object we take it. If it is text, we replace our text with it.
 * We initiate parallel compilation, update the internal editor and restart the external editor
 * if that runs.*/
void CMscGenDoc::DoPasteData(COleDataObject &dataObject)
{
	//Do not insert when in FullScreenViewMode
    if (IsInFullScreenMode()) return;
    bool restartEditor = m_ExternalEditor.IsRunning();
	if (dataObject.IsDataAvailable(m_cfPrivate)) {
		// get file refering to clipboard data
		CFile *pFile = dataObject.GetFileData(m_cfPrivate);
		if (pFile == nullptr) return;
		if (restartEditor)
			m_ExternalEditor.Stop(STOPEDITOR_FORCE);
		// connect the file to the archive and read the contents
		CArchive ar(pFile, CArchive::load);
		ar.m_pDocument = this; // for COleClientItem serialize
		Serialize(ar); //Kills redo part
		ar.Close();
		delete pFile;
    } else if (dataObject.IsDataAvailable(CF_UNICODETEXT)) {
        HGLOBAL hGlobal = dataObject.GetGlobalData(CF_UNICODETEXT);
        size_t length = GlobalSize(hGlobal);
        void* v = GlobalLock(hGlobal);
        if (v==nullptr || length==0) {
            GlobalUnlock(hGlobal);
            GlobalFree(hGlobal);
            return;
        }
        //convert to UTF-8
        std::string text = ConvertFromUTF16_to_UTF8(std::string_view((const char*)v, length));
        GlobalUnlock(hGlobal);
        GlobalFree(hGlobal);
        if (restartEditor) m_ExternalEditor.Stop(STOPEDITOR_FORCE);
        InsertNewChart(CChartData(text.c_str(), m_itrEditing->GetDesign()));  //undo blocked
        m_page_serialized_in = 0; //all pages visible
    } else if (dataObject.IsDataAvailable(CF_TEXT)) {
		HGLOBAL hGlobal = dataObject.GetGlobalData(CF_TEXT);
		size_t length = GlobalSize(hGlobal);
		void* v = GlobalLock(hGlobal);
		if (v==nullptr || length==0) {
			GlobalUnlock(hGlobal);
			GlobalFree(hGlobal);
			return;
		}
		CStringA text;
		text.SetString((char*)v, int(length));
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		if (restartEditor) m_ExternalEditor.Stop(STOPEDITOR_FORCE);
		InsertNewChart(CChartData(text, m_itrEditing->GetDesign()));  //undo blocked
        m_page_serialized_in = 0; //all pages visible
	}
	CompileEditingChart(true, false, false);
    m_page_serialized_in = -1;
	//Copy text to the internal editor
    UpdateInternalEditor();
	if (restartEditor)
		m_ExternalEditor.Start();
};

/** Called by the framework if the user invoked Paste Entire chart.
* We pass it on to CMscGenSrvrItem::CopyToClipboard()*/
void CMscGenDoc::OnEditPasteEntireChart()
{
	COleDataObject dataObject;
	dataObject.AttachClipboard();
	DoPasteData(dataObject);
}

/** Called by the framework to select all the text.
 * We pass it on to the internal editor.*/
void CMscGenDoc::OnEditSelectAll()
{
#ifndef SHARED_HANDLERS
    if (GetInternalEditor())
        theApp.m_pWndEditor->SelectAll();
#endif
}



/** Called by the framework when the user selects the external editor item.
 * (Re-)start the external editor.*/
void CMscGenDoc::OnButtonEdittext()
{
	if (m_ExternalEditor.IsRunning())
		m_ExternalEditor.Stop(STOPEDITOR_WAIT);
    else if (!IsInFullScreenMode())
        m_ExternalEditor.Start();
}

/** Called by the framework to get the status of the external editor control.
 * We enable if the internal editor can run. We check if it is actually running.*/
void CMscGenDoc::OnUpdateButtonEdittext(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_ExternalEditor.IsRunning());
	if (!m_ExternalEditor.CanStart())
		if (!m_ExternalEditor.Init())
			AfxMessageBox(_T("Failed to create external editor window"));
	pCmdUI->Enable(m_ExternalEditor.CanStart() && !IsInFullScreenMode());
}

/** Called by the framework when the user invokes Update Chart.
 * We start a parallel recompilation (cancelling any ongoing)*/
void CMscGenDoc::OnEditUpdate()
{
#ifdef _DEBUG
    //for debug mode we allow recompilation of an unchanged doc
    //update the View, update zoom,
    CompileEditingChart(true, false, true);
#else
    //update the View, update zoom,
    CompileEditingChart(true, false, false);
#endif
}

/**  Called by the framework to get the state of the Update Chart button.
 * We enable if the shown chart is not the same as the compiled one.
 * We also call CMainFrm::TriggerIfRibbonCategoryChange() to see if we have
 * switched away from the Embedded Object category. */
void CMscGenDoc::OnUdpateEditUpdate(CCmdUI *pCmdUI)
{
#ifdef _DEBUG
    //for debug mode we allow recompilation of an unchanged doc
	pCmdUI->Enable(true);
#else
    pCmdUI->Enable(m_itrShown != m_itrEditing);
#endif

#ifndef SHARED_HANDLERS
    //call one of MainFrame's update proc to detect when we switch away from
    //the embedded objects category on the ribbon
    CMainFrame *pMain = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    if (pMain)
        pMain->TriggerIfRibbonCategoryChange();
#endif
}


/** Called by the framework if the user (un)pressed the Track Mode button or
 * invoked track mode via a keyboard shortcut.
 * We enter/leave tracking mode, plus if we entered track mode we call
 * OnInternalEditorSelChange(), to add a track rectange around the element the cursor is inside
 * in the internal editor.*/
void CMscGenDoc::OnButtonTrack()
{
    if (m_pCompilingThread && !m_bTrackMode) return;  //Do not turn TrackMode, when compiling
	SetTrackMode(!m_bTrackMode);
	//This is called if the user pressed the toolbar button, selected the menu or pressed ctrl-T
	//Highlight the rect shown by the editor
	if (m_bTrackMode && IsInternalEditorVisible())
		OnInternalEditorSelChange();
#ifndef SHARED_HANDLERS
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    if (pWnd) {
        UINT nNewStyle = pWnd->m_wndStatusBar.GetPaneStyle(NUM_STATUS_BAR_TACKING);
        if (m_bTrackMode)
            nNewStyle |= SBPS_POPOUT;
        pWnd->m_wndStatusBar.SetPaneStyle(NUM_STATUS_BAR_TACKING, nNewStyle);
    }
#endif
}

/** Called by the framework to get the status of the Track Mode Button.
 * We enable we are not compiling. We set its color depending on whether we are in
 * track mode. We also check if we are in track mode.*/
void CMscGenDoc::OnUpdateButtonTrack(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_bTrackMode);
    pCmdUI->Enable(m_ChartShown.HasElementCovers());
}


void CMscGenDoc::OnTrackHere()
{
    StartFadingAll();
#ifndef SHARED_HANDLERS
    auto [line, col] = theApp.m_pWndEditor->m_ctrlEditor.GetMenuLineCol();
    auto tracked_arc = m_ChartShown.GetArcByLine(line+1, col+1);
    AnimationElement *a = AddAnimationTrackRect(tracked_arc);
    if (a) {
        if (m_bTrackMode) {
            a->InvertKeep();
            if (DoFading())
                StartFadingTimer();
        }
    }
#endif
}

void CMscGenDoc::OnUpdateTrackHere(CCmdUI * pCmdUI)
{
#ifndef SHARED_HANDLERS
    if (IsInternalEditorVisible()) {
        auto [line, col] = GetInternalEditor()->m_ctrlEditor.GetMenuLineCol();
        auto tracked_arc = m_ChartShown.GetArcByLine(line+1, col+1);
        pCmdUI->Enable(tracked_arc!=nullptr);
    } else
        pCmdUI->Enable(false);
#endif
}

void CMscGenDoc::OnButtonCollapseAll()
{
    if (m_pCompilingThread) return; //skip if compiling
    std::string new_state = m_ChartShown.GetAllCollapsedGUIState();
    if (new_state.length()==0 || new_state == m_ChartShown.m_GUIState) return; // no change
    CChartData chart(*m_itrEditing);
    chart.block_undo = true;
    chart.m_GUIState = new_state;
    InsertNewChart(chart);
    CompileEditingChart(true, false, false);
}

void CMscGenDoc::OnButtonExpandAll()
{
    if (m_pCompilingThread) return; //skip if compiling
    std::string new_state = m_ChartShown.GetAllExpandedGUIState();
    if (new_state.length()==0 || new_state == m_ChartShown.m_GUIState) return; // no change
    CChartData chart(*m_itrEditing);
    chart.block_undo = true;
    chart.m_GUIState = new_state;
    InsertNewChart(chart);
    CompileEditingChart(true, false, false);
}



/** Called by the framework when the user invokes next error (F8).
 * We pass it on to DoViewNexterror()*/
void CMscGenDoc::OnViewNexterror()
{
	DoViewNexterror(true, false);
}

/** Called by the framework when the user invokes prev error (Shift+F8).
* We pass it on to DoViewNexterror()*/
void CMscGenDoc::OnViewPreverror()
{
	DoViewNexterror(false, false);
}

/** Called by the framework when the user invokes next error (Ctrl+F8).
 * We pass it on to DoViewNexterror()*/
void CMscGenDoc::OnViewNexterrorSmall()
{
	DoViewNexterror(true, true);
}

/** Called by the framework when the user invokes prev error (Shift+Ctrl+F8).
* We pass it on to DoViewNexterror()*/
void CMscGenDoc::OnViewPreverrorSmall()
{
	DoViewNexterror(false, true);
}


/** Select the next/prev error in the error window.
 * Jump there in the internal/external editor(s), plus flash its area.*/
void CMscGenDoc::DoViewNexterror(bool next, bool small_step)
{
#ifndef SHARED_HANDLERS
    COutputViewBar *pOutputView = theApp.m_pWndOutputView;
	if (!pOutputView) return;

	//Move to next error
    pOutputView->NextError(next, small_step);
    //Try to jump there in the input (or make noise)
    pOutputView->JumpToCurrentError(false);
#endif
}

/** Change the forced design of the chart.
 * If we invoke a real change, we create a new undo point & recompile.*/
void CMscGenDoc::ChangeDesign(const char *design)
{
	if (!strcmp(design,m_itrEditing->GetDesign())) return;
	InsertNewChart(CChartData(*m_itrEditing)); //duplicate current chart, loose undo, keep page shown
	m_itrEditing->SetDesign(design);
    m_itrEditing->block_undo = true;
    auto pConf = GetConf();
    if (!pConf) return;
    pConf->m_languages.cshParams->ForcedDesign = design;
	CompileEditingChart(true, false, false);
#ifndef SHARED_HANDLERS
    if (IsInternalEditorVisible())
        theApp.m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
#endif
}

/** Select a new page of the chart.
 * If we are compiling, we ignore it.
 * If we invoke a real change, we change page, update views and
 * refresh the text in the page selector combo box (so we create the
 * standard "X/Y" look where X is the current page and Y is the total
 * number of pages.*/
void CMscGenDoc::ChangePage(unsigned page)
{
    //skip if compiling
    if (m_pCompilingThread) return;
    if (page != m_ChartShown.GetPage()) {
        m_ChartShown.SetPage(page);
#ifndef SHARED_HANDLERS
        CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
        if (pWnd && pWnd->m_bAutoSplit)
            pWnd->SetSplitSize(unsigned(m_ChartShown.GetHeadingSize()*m_zoom/100.));
        if (!ArrangeViews())
            UpdateAllViews(nullptr); //if zoom needed not be changed, we call update to show the new page
#endif
        CheckIfChanged();
        NotifyChanged();
    }
#ifndef SHARED_HANDLERS
    if (IsInternalEditorVisible())
        theApp.m_pWndEditor->m_ctrlEditor.SetFocus();
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    if (pWnd)
        pWnd->FillPageComboBox(m_ChartShown.GetPages(), page);
#endif
}

/** Increase or decrease page count.
 * We calculate the new page number and invoke ChangePage().*/
void CMscGenDoc::StepPage(signed int step)
{
	if (step == 0 || m_ChartShown.GetPages()<=1)
		return;
    int page = m_ChartShown.GetPage() + step;
    if (page > int(m_ChartShown.GetPages())) page = m_ChartShown.GetPages();
    if (page < 0 ) page = 0;
    if (page == int(m_ChartShown.GetPage())) return;
    ChangePage(page);
}


/** Set zoom factor.
 * If we are compiling, we ignore it.
 * If we invoke a real change, update views and
 * refresh the text in the zoom selector combo box (so we create the
 * standard "xy%" look.
 * Returns true if actual change happened
 * @param [in] zoom The current zoom value, not adjusted for DPI scaling,
 *                  that is, a value of 100 means one pixel for each
 *                  chart pixel. When showing to the user we divide it by
 *                  the DPI scaling factor.*/
bool CMscGenDoc::SetZoom(int zoom)
{
    if (m_pCompilingThread) return false; //skip if compiling
	if (zoom < 1) zoom = m_zoom;
	if (zoom > int(m_maxZoom)) zoom = m_maxZoom;
	if (zoom < int(m_minZoom)) zoom = m_minZoom;

	if (zoom == m_zoom) return false;
	m_zoom = zoom;
    UpdateAllViews(nullptr);
    //Save zoom mode - and set it to no automatic zoom setting
    //If we have AutoSplit, this would call OnSize and trigger an
    //Automatic zoom setting - defeating the purpose of this call.
    EZoomMode save = m_ZoomMode;
    m_ZoomMode = EZoomMode::NONE;
#ifndef SHARED_HANDLERS
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
	if (pWnd)
        pWnd->FillZoomComboBox(m_zoom);
#endif
    m_ZoomMode = save;
    return true;
}


/** Automatically adjusts zoom factor according to 'mode'.
 * We ignore if we compile.
* If we invoke a real change, update views and
* refresh the text in the zoom selector combo box(so we create the
* standard "xy%" look.
* Returns true if actual change happened*/
bool CMscGenDoc::ArrangeViews(EZoomMode mode)
{
#ifdef SHARED_HANDLERS
    return false;
#else
    if (m_pCompilingThread) return false; //skip if compiling
	if (mode == EZoomMode::NONE) return false;
	POSITION pos = GetFirstViewPosition();
	if (pos == nullptr) return false;
    CMscGenView* pView = dynamic_cast<CMscGenView*>(GetNextView(pos));
  	if (!pView) return false;
    const CSize size = m_ChartShown.GetSize(m_ChartShown.GetPage());
	if (size.cx==0 || size.cy==0) return false;
	CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
	if (!pWnd) return false;

	//Query size of the view, the main window and the screen
	RECT view, window, screen;
	pView->GetClientRect(&view);
	pWnd->GetWindowRect(&window);
	CClientDC dc(nullptr);
	dc.GetClipBox(&screen);
	//calculate the space available to a view on the screen
	unsigned x, y;
	x = screen.right-screen.left;
	y = screen.bottom-screen.top;
	//For a full screen window, the entire screen is there
	//If not full screen, we reduce the sizes with a MARGIN and the elements of the
	//main window outside the view (toolbars, title, menus, border, docking windows, etc.)
	if (!pWnd->IsFullScreen()) {
        const unsigned SCREEN_MARGIN = 50;
		x -= (window.right-window.left) - (view.right-view.left) + SCREEN_MARGIN;
		y -= (window.bottom-window.top) - (view.bottom-view.top) + SCREEN_MARGIN;
	}
	int zoom = m_zoom;
	//OK, we have a sane View, with some drawing in it and we are not in place with a sane main window
    switch (mode) {
		case EZoomMode::OVERVIEW:
			//Kill all, but one view
			for (int i = 1; i<pWnd->m_wndSplitter.GetRowCount(); i++)
				pWnd->m_wndSplitter.DeleteRow(i);
            //HACK: Update this
			//Re-query view size
			pos = GetFirstViewPosition();
			pView = dynamic_cast<CMscGenView*>(GetNextView(pos));
  			if (!pView) return false;
			pView->GetClientRect(&view);

			//See which dimension is limiting
            if (double(view.bottom - view.top)/double(view.right - view.left) > double(size.cy)/double(size.cx))
				zoom = unsigned(double(view.right - view.left)/double(size.cx)*100.);
			else
				zoom = unsigned(double(view.bottom - view.top)/double(size.cy)*100.);
			if (zoom > m_maxAutoZoom) zoom = m_maxAutoZoom;
			break;
        case EZoomMode::ORIGSIZE:
            zoom = int(100*GetConf()->GetCurrentDPIScalingFactor());
            break;
		case EZoomMode::ZOOM_FITTOWIDTH:
			zoom = unsigned((view.right-view.left)*100./size.cx);
			if (zoom>m_maxAutoZoom) zoom = m_maxAutoZoom;
			break;
	}
    return SetZoom(zoom); //if true, we also call UpdateAllViews from SetZoom();
#endif
}

/** Called by the framework when the user presses Overview button.
 * We pass it on to ArrangeViews()*/
void CMscGenDoc::OnViewZoomnormalize()
{
	ArrangeViews(EZoomMode::OVERVIEW);
}

/** Called by the framework when the user presses Original Size button.
* We pass it on to ArrangeViews()*/
void CMscGenDoc::OnView100Percent()
{
    ArrangeViews(EZoomMode::ORIGSIZE);
}

/** Called by the framework when the user presses Fit Width button.
* We pass it on to ArrangeViews()*/
void CMscGenDoc::OnViewFittowidth()
{
    ArrangeViews(EZoomMode::ZOOM_FITTOWIDTH);
}

/** Called by the framework to get the status of the Zoom buttons.
* We enable if we do not compile.*/
void CMscGenDoc::OnUpdateViewZoom(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(m_pCompilingThread==nullptr);
}

/** Sets the zoom mode to 'mode'.
 * Writes registry and adjusts zoom, as well.*/
void CMscGenDoc::SwitchZoomMode(EZoomMode mode)
{
	if (m_ZoomMode == mode)
		m_ZoomMode = EZoomMode::NONE;
	else
		m_ZoomMode = mode;
	ArrangeViews(m_ZoomMode);
	AfxGetApp()->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_DEFAULTZOOMMODE, (int)m_ZoomMode);
}

/** Called by the framework when the user sets the Overview checkbox.
* We pass it on to ArrangeViews()*/
void CMscGenDoc::OnZoommodeKeepinoverview()
{
	SwitchZoomMode(EZoomMode::OVERVIEW);
}

/** Called by the framework when the user sets the Original Size checkbox.
* We pass it on to ArrangeViews()*/
void CMscGenDoc::OnZoommodeKeep100Percent()
{
	SwitchZoomMode(EZoomMode::ORIGSIZE);
}

/** Called by the framework when the user sets the Fit Width checkbox.
* We pass it on to ArrangeViews()*/
void CMscGenDoc::OnZoommodeKeepfittingtowidth()
{
	SwitchZoomMode(EZoomMode::ZOOM_FITTOWIDTH);
}

/** Called by the framework to get the status of the Overview checkbox.
* We enable if we do not compile, check if this mode is selected.*/
void CMscGenDoc::OnUpdateZoommodeKeepinoverview(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_ZoomMode == EZoomMode::OVERVIEW);
    pCmdUI->Enable(m_pCompilingThread==nullptr);
}

/** Called by the framework to get the status of the original size checkbox.
* We enable if we do not compile, check if this mode is selected.*/
void CMscGenDoc::OnZoommodeKeep100Percent(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_ZoomMode == EZoomMode::ORIGSIZE);
    pCmdUI->Enable(m_pCompilingThread==nullptr);
}

/** Called by the framework to get the status of the Fit Width checkbox.
* We enable if we do not compile, check if this mode is selected.*/
void CMscGenDoc::OnUpdateZoommodeKeepfittingtowidth(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_ZoomMode == EZoomMode::ZOOM_FITTOWIDTH);
    pCmdUI->Enable(m_pCompilingThread==nullptr);
}

/** Called by everyone when a mouse wheel event happens.
 * Unfortunately Windows routes mouse wheel events to the window that has the focus
 * as opposed to where the pointer is. In order to do the right thing, we forward all
 * mouse events to this function, which figures out based on the mouse pointer,
 * who should receive it and calls the appropriate DoMouseWheel() function.
 * Return true if the event is handled.*/
bool CMscGenDoc::DispatchMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	POSITION pos = GetFirstViewPosition();
	while (pos != nullptr) {
        CMscGenView* pView = dynamic_cast<CMscGenView*>(GetNextView(pos));
  	    if (pView)
			//Each view handles this message only if pt is within its area
	        if (pView->DoMouseWheel(nFlags, zDelta, pt)) return true;
	}
	//None of the views had it, try the internal editor
#ifndef SHARED_HANDLERS
    if (IsInternalEditorVisible())
		return theApp.m_pWndEditor->m_wndSplitter.DoMouseWheel(nFlags, zDelta, pt);
#endif
	return false;
}


/**Inserts new chart after the current one and deletes redo list.*/
void CMscGenDoc::InsertNewChart(const CChartData &data)
{
	//The Visual Studio stl implementation changes the value of .end() at insert.
	//Re-set m_itr* to .end() after list manipulation, if it was .end() before
	//or if we delete it now
	bool saved_chart_is_deleted = m_itrSaved == m_charts.end();
	bool shown_chart_is_deleted = m_itrShown == m_charts.end();
	IChartData i;
	for (i = ++IChartData(m_itrEditing); i!=m_charts.end(); i++) {
		if (i == m_itrSaved) saved_chart_is_deleted = true;
		if (i == m_itrShown) shown_chart_is_deleted = true;
	}
	m_charts.erase(++IChartData(m_itrEditing), m_charts.end());
	m_charts.push_back(data);
	m_itrEditing = --m_charts.end();
	if (saved_chart_is_deleted)
		m_itrSaved = m_charts.end();
	if (shown_chart_is_deleted)
		m_itrShown = m_charts.end();
}

/** Check if the chart has changed compared to the last saved version.
 * For text files, it is the text that is compared.
 * In case of an embedded chart page, design, entity or box collapse, fallback resolution or
 * page break indicator changes also result in changed state, since we save these. */
bool CMscGenDoc::CheckIfChanged()
{
    if (IsInFullScreenMode()) goto not_modified;
	if (m_itrSaved == m_charts.end()) goto modified; //not yet saved
	if (IsEmbedded()) {
        auto pConf = GetConf();
        if (!pConf) return false;
        if (m_uSavedFallbackResolution != pConf->m_uFallbackResolution) goto modified;
        if (m_bSavedPageBreaks != pConf->m_bPageBreaks) goto modified;
        if (m_uSavedPage != m_ChartShown.GetPage()) goto modified;
    }
	if (m_itrSaved != m_itrEditing) {
		if (m_itrSaved->GetText() != m_itrEditing->GetText()) goto modified;
		if (IsEmbedded()) {
			if (m_itrSaved->GetDesign() != m_itrEditing->GetDesign()) goto modified;
            if (!m_itrSaved->IsGUIStateSameAsIn(*m_itrEditing)) goto modified;
		}
	}
not_modified:
	SetModifiedFlag(FALSE);
	return false;
modified:
	SetModifiedFlag(TRUE);
    return true;
}

void CMscGenDoc::SetModifiedFlag(BOOL bModified)
{
    if (IsModified() != bModified) {
        COleServerDocEx::SetModifiedFlag(bModified);
        UpdateFrameCounts();
        CString title = GetTitle();
        int asterisk = title.GetLength()-1;
        if (asterisk >=0 && title.GetAt(asterisk)=='*') {
            if (!bModified)
                title.SetAt(asterisk, 0);
        } else if (bModified)
            title += '*';
        SetTitle(title);
    }
}

/** Called by the external editor window if the text in the external editor is saved.
 * We insert a new undo block, update internal editor and start a parallel compile.*/
void CMscGenDoc::OnExternalEditorChange(const CChartData &data)
{
    SetTrackMode(false);
    m_itrEditing->block_undo = true;
#ifndef SHARED_HANDLERS
    if (GetInternalEditor()) {
        theApp.m_pWndEditor->m_ctrlEditor.GetSel(m_itrEditing->m_sel);
        ::SendMessage(theApp.m_pWndEditor->m_ctrlEditor.m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&m_itrEditing->m_scroll);
    }
#endif
    InsertNewChart(data);
    m_itrEditing->block_undo = true;
#ifndef SHARED_HANDLERS
    if (GetInternalEditor()) {
        theApp.m_pWndEditor->m_ctrlEditor.GetSel(m_itrEditing->m_sel);
        ::SendMessage(theApp.m_pWndEditor->m_ctrlEditor.m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&m_itrEditing->m_scroll);
    }
#endif
    CompileEditingChart(true, false, false);
    UpdateInternalEditor();
}

/** Called by the Internal editor, when its text changes.
 * We kill track mode and all fade all elements. Then we figure out if this
 * change can be appended to the last undo block (continued insertion or deletion).
 * Then we append it or open a new undo block.
 * @param [in] start The first character to change (zero indexed)
 * @param [in] del The number of characters deleted.
 * @param [in] ins The number of characters inserted (after the delete)
 * @param [in] sel_before The cursor position before the change.
 * @param [in] scroll_pos_before The scollbar positions before the change.*/
void CMscGenDoc::OnInternalEditorChange(long start, long ins, long del, CHARRANGE sel_before, POINT scroll_pos_before)
{
	//SetTrackMode invalidates if tracking is turned off
	//It may be that tracking was already off, but we ahd track rects shown (e.g., due to displaying a warning or error)
	//In that case we meed to invalidate (clear the retcs) here.
	if (m_bTrackMode)
		SetTrackMode(false);
	else
		StartFadingAll();
#ifndef SHARED_HANDLERS
    ASSERT(GetInternalEditor());
    CStringA text = theApp.m_pWndEditor->m_ctrlEditor.GetUtf8TextAsCStringA();

    //First decide if we append this change to the last undo buffer or
    //create a new point.
    bool append;
    if (m_charts.size()==0)
        append = false;
    else if (m_itrEditing->block_undo)
        append = false;
    else if (ins>0 && del>0)
        append = false;
    else if (m_itrEditing->ins>0 && m_itrEditing->del>0)
        append = false;
    else if (m_itrEditing->ins>0 && del>0)
        append = false;
    else if (m_itrEditing->del>0 && ins>0)
        append = false;
    else if (ins>0) {
        //OK we insert - did we do this to the end of the undo record's insert
        if (m_itrEditing->start + m_itrEditing->ins == start) {
            append = true;
            m_itrEditing->ins += ins;
        } else
            append = false;
    } else if (del>0) {
        //OK we deleted - did we do this at the end of the undo record's insert
        if (m_itrEditing->start == start) {
            append = true;
            m_itrEditing->del += del;
        } else if (m_itrEditing->start == start + del) {
            append = true;
            m_itrEditing->start -= del;
        } else
            append = false;
    } else
        append = false;

    if (text != m_itrEditing->GetText()) {
        if (append) {
            CStringA fd = m_itrEditing->GetDesign();
            m_itrEditing->Set(std::move(text));
            m_itrEditing->SetDesign(fd); //Set destroys fd and cr
            m_itrShown = m_charts.end(); //so that it is different from m_itrEditing and we compile if user presses F2
        } else {
            //block undo of previous. We no longer add to it even if we later undoed
            //back to it. Plus store the selection before the change that triggered us.
            if (m_charts.size()) {
                m_itrEditing->block_undo = true;
                m_itrEditing->m_sel = sel_before;
                m_itrEditing->m_scroll = scroll_pos_before;
            }
            CChartData chart(std::move(text), m_itrEditing->GetDesign());
            chart.CopyGUIStateFrom(*m_itrEditing);
            InsertNewChart(chart);
            m_itrEditing->block_undo = false;
            m_itrEditing->start = start;
            m_itrEditing->ins = ins;
            m_itrEditing->del = del;
        }
        //'text' is emptied by now.
        //finally set the cursor & scrollbar pos in the freshly inserted entry to where it is now.
        theApp.m_pWndEditor->m_ctrlEditor.GetSel(m_itrEditing->m_sel);
        ::SendMessage(theApp.m_pWndEditor->m_ctrlEditor.m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&m_itrEditing->m_scroll);
        CheckIfChanged();
    }
#endif
}

/** Called by the internal editor if the user changes selection (e.g., moves the cursor).
 * It is meticously not called during coloring (after which we restore the original cursor
 * position). It is also not if a mouse move changes the selection during tack mode.
 * We only react in tracking mode by animating the coverage of the new arc we move to.*/
void CMscGenDoc::OnInternalEditorSelChange()
{
    m_saved_charrange.cpMin = -1;
	if (!m_bTrackMode || !IsInternalEditorVisible()) return;
#ifndef SHARED_HANDLERS
	long start, end;
	theApp.m_pWndEditor->m_ctrlEditor.GetSel(start, end);
	int line, col;
	theApp.m_pWndEditor->m_ctrlEditor.ConvertPosToLineCol(start, line, col);
	//Add new track arc and update the views if there is a change
	StartFadingAll();
    AnimationElement *a = AddAnimationTrackRect(m_ChartShown.GetArcByLine(line+1, col+1));
    if (a && !a->keep)
        a->target_fade = AnimationElement::TRACK_MODE_HOOVER_FADE_PERCENT/100.;
#endif
}

/** Thread function for the compilation thread.
 * This function is called when starting the compilation thread.
 * We lock the compilation section and attempt to compile.
 * If compilation throws an AbortCompilingException exception,
 * we kill everything and unlock.
 * If we succeed, we send a message to the Main Frame window
 * and exit.
 * @param pParam A pointer to the CMscGenDoc we are compiling.
 * @returns 1 on aborted compilation and 0 on success.*/
UINT CompileThread( LPVOID pParam )
{
    CMscGenDoc *pDoc = (CMscGenDoc*)pParam;
    pDoc->m_SectionCompiling.Lock();
    try {
        pDoc->m_ChartCompiling.CompileIfNeeded();
    } catch (AbortCompilingException) {
        pDoc->m_ChartCompiling.Delete();
        pDoc->m_killCompilation = false;
        pDoc->m_SectionCompiling.Unlock();
        return 1;
    }
    //If the compilation was aborted, act so
    if (pDoc->m_ChartCompiling.IsCompiled()) {
        pDoc->m_SectionCompiling.Unlock();
        if (pDoc->m_hMainWndToSignalCompilationEnd)
            PostMessage(pDoc->m_hMainWndToSignalCompilationEnd, WM_APP+293, 0, 0);
        return 0;
    } else {
        pDoc->m_ChartCompiling.Delete();
        pDoc->m_killCompilation = false;
        pDoc->m_SectionCompiling.Unlock();
        return 1;
    }
}

/** A progress callback for blocking compilation.
 * This is used when compilation is not done in a separate thread, but as
 * part of the main thread. (We call that blocking compilation as the user
 * is blocked to do anything during it.)
 * We check if someone has set CMscGenDoc::m_killCompilation and throw if so.
 * Then we update the progress animation and force a repaint of the view.
 * @param [in] progress A pointer to the progress object, where we can ask for current progress.
 *             NULL if no such object.
 * @param [in] data A pointer to the CMscGenDoc we are compiling.
 * @param [in] a The method by which the chart calling this wants to receive
 *             an abort signal.
 * @returns true if the compilation shall stop. Not all charts honour this.*/
bool ProgressCallbackBlocking(const ProgressBase*progress, void *data,
                              ProgressBase::EPreferredAbortMethod a)
{
    CMscGenDoc *pDoc = (CMscGenDoc *)data;
    pDoc->m_Progress = progress;
    if (pDoc->m_killCompilation) {
        if (a==ProgressBase::EXCEPTION)
            throw AbortCompilingException();
        else if (a==ProgressBase::RETVAL)
            return true;
    }
    pDoc->DoFading();
	POSITION pos = pDoc->GetFirstViewPosition();
	while(pos) {
		CMscGenView* pView = dynamic_cast<CMscGenView*>(pDoc->GetNextView(pos));
        if (pView) {
            pView->Invalidate();
            pView->ForceRepaint();
        }
	}
    return false;
}

/** A progress callback for non-blocking compilation.
 * This is used when compilation is done in a separate thread.
 * (We call that non-blocking compilation as the user is not blocked
 * and can do anything during it.)
 * We check if someone has set CMscGenDoc::m_killCompilation and if so, we
 *    a) throw AbortCompilingException if a == EXCEPTION
 *    b) return true if a==RETVAL
 * Then we update the progress variable and let the view repaint itself when
 * the animation timer kicks in next.
 * @param [in] progress The progress object tracking where we are.
 * @param [in] data A pointer to the CMscGenDoc we are compiling.
 * @param [in] a The method by which the chart calling this wants to receive
 *             an abort signal.
 * @returns true if compilation shall be stopped AND a==RETVAL.*/
bool ProgressCallbackNonBlocking(const ProgressBase*progress, void *data,
                                 ProgressBase::EPreferredAbortMethod a)
{
    CMscGenDoc *pDoc = (CMscGenDoc *)data;
    pDoc->m_Progress = progress;
    if (pDoc->m_killCompilation) {
        if (a==ProgressBase::EXCEPTION)
            throw AbortCompilingException();
        else if (a==ProgressBase::RETVAL)
            return true;
    }
    return false;
}

/** Compiles the currently edited chart text.
 * This is the main compilation function.
 * We do nothing if we are an OLE object in full screen view mode.
 * We do nothing if this text has already been compiled, unless we are forced.
 * We try to do compilation on a separate thread, if we can, that is, if
 * 'force_block' is false, our main window exists and is visible; and
 * we are not attempting to close.
 * If we compile, we kill the ongoing compilation process, set up m_ChartCompiling,
 * with the right text and parameters.
 * Then we kill all ongoing animations and add a single COMPILING_GREY one, which
 * will display the progress bar. This will be refreshed automatically via a timer in
 * CMscGenView as any othe animation. We disable the scroll bars.
 * Then if we do a non-blocking compilation, we start the compilation thread and return.
 * (The compilation thread will send a message to the Main Window when succeeded, which will
 * call CMscGenDoc::CompleteCompilingEditingChart() in return.)
 * Else we perform all of the compile and pass control to CMscGenDoc::CompleteCompilingEditingChart().
 * @param [in] resetZoom We set m_hadArrangeViews to this value. If true, this will cause the zoom
 *                       to get automatically adjusted to the zoom mode after compilation.
 * @param [in] force_block If true, force compilation to be blocking, even if we could do a
 *                         non-blocking one. Useful, when caller wants to have the result of the
 *                         compilation sychronously.
 * @param [in] force_compile If true, we recompile even if apparently nothing has changed. This
 *                           is useful, if some external parameter has changed, such as page size.*/
void CMscGenDoc::CompileEditingChart(bool resetZoom, bool force_block, bool force_compile)
{
    //Change showing only if not viewing in full screen mode
    if (IsInFullScreenMode() && m_charts.size()>1) return;

    //If we are already compiled (or already compiling this text) - stop
    if (m_itrShown == m_itrEditing && !force_compile)
        return;

    m_itrEditing->RemoveSpacesAtLineEnds();
    m_itrShown = m_itrEditing;
#ifdef SHARED_HANDLERS
    const bool block = true;
#else
    if (GetInternalEditor()) {
        //Save new selection in character index terms
        theApp.m_pWndEditor->m_ctrlEditor.GetSel(m_itrEditing->m_sel);
        ::SendMessage(theApp.m_pWndEditor->m_ctrlEditor.m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&m_itrEditing->m_scroll);
    }
    //if the main window is invisible, do a blocking compilation
    //Needed for single thread operation when the system DLLs start our
    //application to draw an embedded object, for instance.
    //This can be nil if we are shutting down...
    CMainFrame *pWnd = m_bAttemptingToClose ? nullptr : dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    const bool block = force_block || !pWnd || !pWnd->IsWindowVisible();

    if (pWnd) {
        //See if we have the (potentially) new ForcedDesign verified & copied to the combo box of DesignBar
        if (!pWnd->FillDesignComboBox(m_itrShown->GetDesign(), false, false))
            m_itrShown->SetDesign("");
        pWnd->FillLayoutComboBox(m_itrShown->GetLayout(), false);
    }
#endif

    KillCompilation();

    //Prepare m_ChartCompiling
    auto pConf = GetConf();
    if (!pConf) return;
    m_ChartCompiling = *m_itrShown;
    m_ChartCompiling.SetFileName(GetPathName()); //empty for embedded and (Unnamed)
    if (m_page_serialized_in>=0)
        m_ChartCompiling.SetPage(m_page_serialized_in);
    else
        m_ChartCompiling.SetPage(m_ChartShown.GetPage());
    m_page_serialized_in = -1;
    m_ChartCompiling.SetFallbackResolution(pConf->m_uFallbackResolution);
    m_ChartCompiling.SetPageBreaks(pConf->m_bPageBreaks);
#ifndef SHARED_HANDLERS
    if (pWnd) {
        const CDrawingChartData::ECacheType should_be = pWnd->m_at_embedded_object_category ?
                        CDrawingChartData::CACHE_EMF : CDrawingChartData::CACHE_RECORDING;
        if (pWnd && pWnd->m_at_embedded_object_category &&
            m_ChartShown.GetCacheType()!=CDrawingChartData::CACHE_EMF)
            m_highlight_fallback_images = true;
        m_ChartCompiling.SetCacheType(should_be);
    }
#endif
    m_ChartCompiling.SetProgressCallback(block ? ProgressCallbackBlocking : ProgressCallbackNonBlocking, this);
    m_ChartCompiling.SetPedantic(pConf->m_Pedantic);
    if (pConf->m_bSkipCopyright)
        m_ChartCompiling.SetCopyRightText("");
    else
        m_ChartCompiling.SetCopyRightText(pConf->m_CopyrightText);
    if (!pConf->m_bAutoPaginate)
        m_ChartCompiling.SetPageSize(XY(0,0));
    else if (pConf->m_iScale4Pagination<=0)
        m_ChartCompiling.SetPageSize(pConf->GetPrintablePaperSize());
    else
        m_ChartCompiling.SetPageSize(pConf->GetPrintablePaperSize()*(100./pConf->m_iScale4Pagination));
    m_ChartCompiling.SetAddHeading(pConf->m_bAutoHeading);
    m_ChartCompiling.SetFitWidth(pConf->m_iScale4Pagination == -1);
    std::string load_data = (const char*)AsUTF8(GetRegistryString(REG_KEY_LOAD_DATA, L""));
    m_ChartCompiling.m_load_data = SplitLoadData(&load_data)[pConf->m_lang->GetName()].c_str();

	//All tracking rectangles, but add grey area
	SetTrackMode(false);
    m_animations.clear();
    m_controlsShowing.clear();
    AddAnimationCompilingGray();

    m_hadArrangeViews = resetZoom;
    m_killCompilation = false;
    m_Progress = 0;
#ifdef SHARED_HANDLERS
    m_hMainWndToSignalCompilationEnd = nullptr;
#else
    if (block)
        m_hMainWndToSignalCompilationEnd = nullptr;
    else
        m_hMainWndToSignalCompilationEnd = *pWnd;
#endif
    //Disable scroll bars
	POSITION pos = GetFirstViewPosition();
	while(pos) {
		CMscGenView* pView = dynamic_cast<CMscGenView*>(GetNextView(pos));
        if (pView) {
            pView->EnableScrollBarCtrl(SB_BOTH, FALSE);
            pView->UpdateWindow();
        }
	}
#ifndef SHARED_HANDLERS
    if (pWnd) {
        pWnd->SetProgressBarPosition(0);
        pWnd->SetProgressBarState(TBPF_NORMAL);
    }
    if (!block) {
        //Fire up compilation thread
        m_pCompilingThread = AfxBeginThread(CompileThread, (LPVOID)this);

        //Wait till compilation truly begins
        while (m_Progress==0 && !m_ChartCompiling.IsCompiled())
            Sleep(10);

        //If there is an internal editor, reset focus to it.
        if (IsInternalEditorVisible())
            theApp.m_pWndEditor->m_ctrlEditor.SetFocus();
    } else {
        CWaitCursor wait;
        m_ChartCompiling.CompileIfNeeded();
        CompleteCompilingEditingChart();
    }
#else
    CWaitCursor wait;
    m_ChartCompiling.CompileIfNeeded();
    CompleteCompilingEditingChart();
#endif
}


/** This function receives control after compilation for both blocking and
 * non-blocking compilation.
 * We emit load measurement data, swap in the compiled char to m_ChartShown,
 * tidy up, update an embedded chart's container document and kill animations (the progress bar).
 * Then we update ribbon fields with the new zoom, design and page number (may have changed).
 * Then if we are at the Embedded Object category, we fill in object size and start a small
 * animation for fallback images.
 * Then we adjust zoom (if requested in CompileEditingChart) and update the view (repaint).
 * Finally we copy the resuling errors to the error window and leave the focus at the internal editor.*/
void CMscGenDoc::CompleteCompilingEditingChart()
{
    m_SectionCompiling.Lock();
    _ASSERT(m_ChartCompiling.IsCompiled());
    auto pConf = GetConf();
    if (!pConf) return;
    if (m_ChartCompiling.m_load_data.GetLength()) {
        std::string load_data = (const char *)AsUTF8(GetRegistryString(REG_KEY_LOAD_DATA, L""));
        auto ldset = SplitLoadData(&load_data);
        ldset[pConf->m_lang->GetName()] = m_ChartCompiling.m_load_data;
#ifndef SHARED_HANDLERS
        AfxGetApp()->WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_LOAD_DATA,
                                        AsUnicode(PackLoadData(ldset)));
#endif
    }
    m_ChartShown.swap(m_ChartCompiling);
    m_ChartCompiling.Delete();
    m_pCompilingThread = nullptr;
    m_killCompilation = false;
    m_SectionCompiling.Unlock();

    if (CheckIfChanged())
        NotifyChanged();

    if (m_animations.size()!=1 || m_animations.begin()->what!=AnimationElement::COMPILING_GREY)
        _ASSERT(0);
    if (m_bAttemptingToClose) return;
    StartFadingAll(); //start fading the grey area
#ifndef SHARED_HANDLERS
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    if (pWnd) {
        pWnd->FillDesignComboBox(m_ChartShown.GetDesign(), false, false);
        pWnd->FillLayoutComboBox(m_ChartShown.GetLayout(), false);
        pWnd->FillPageComboBox(m_ChartShown.GetPages(), m_ChartShown.GetPage());
        pWnd->FillZoomComboBox(m_zoom);
        //If we show the WMF, let us update the embedded object size shown in the ribbon
        if (m_ChartShown.GetCacheType() == CDrawingChartData::CACHE_EMF
            && pWnd->m_at_embedded_object_category) {
                const size_t size = m_ChartShown.GetWMFSize() + serialize_doc_overhead +
                                    m_ChartShown.GetText().GetLength();
                const double total = double(m_ChartShown.GetSize().cx)*m_ChartShown.GetSize().cy;
                pWnd->FillEmbeddedPanel(size, 100*m_ChartShown.GetWMFFallbackImagePos().GetArea() / total);
        }
        if (m_highlight_fallback_images) {
            StartTrackFallbackImageLocations(m_ChartShown.GetWMFFallbackImagePos());
            m_highlight_fallback_images = false;
        }
        pWnd->m_mscgen_mode_ind = m_ChartShown.GetMscgenCompat();
        pWnd->SetProgressBarState(TBPF_NOPROGRESS);
    }
    //SetZoom(); //reset toolbar
    if (m_hadArrangeViews)
        ArrangeViews();
    UpdateAllViews(nullptr);
    //Display error messages
    if (theApp.m_pWndOutputView) {
        theApp.m_pWndOutputView->ShowCompilationErrors(m_ChartShown);
        theApp.m_pWndOutputView->JumpToCurrentError(true);
    }

    //If there is an internal editor, reset focus to it.
    if (IsInternalEditorVisible())
        theApp.m_pWndEditor->m_ctrlEditor.SetFocus();
#endif
}


/** Kill any ongoing compilation - a blocking call.
 * We wait till the compilation thread aborts. */
void CMscGenDoc::KillCompilation()
{
    m_killCompilation = true; //this causes the thread to stop
    m_SectionCompiling.Lock(); //wait for thread to exit
    m_ChartCompiling.Delete();
    m_pCompilingThread = nullptr;
    m_killCompilation = false;
    m_SectionCompiling.Unlock();
    if (m_bAttemptingToClose) return;
    StartFadingAll(); //start fading the grey area
#ifndef SHARED_HANDLERS
    //Kill taskbar progress indication
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    if (pWnd)
        pWnd->SetProgressBarState(TBPF_NOPROGRESS);
    //If there is an internal editor, reset focus to it.
    if (IsInternalEditorVisible())
        theApp.m_pWndEditor->m_ctrlEditor.SetFocus();
#endif
}

/** A blocking call to wait till the compilation ends.
 * Returns immediately if there is no compilation ongoing.*/
void CMscGenDoc::WaitForCompilationToEnd()
{
    m_SectionCompiling.Lock(); //wait for thread to exit
    m_SectionCompiling.Unlock();
}

void CMscGenDoc::RecoverAutoSavedFile(const CString & filename)
{
#ifndef SHARED_HANDLERS
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(theApp.GetMainWnd());
    bool insert_at_cursor = !GetConf()->m_bRecoveryOverwrites && IsInternalEditorVisible();
    bool restartEditor = false;
    auto pConf = GetConf();
    if (!pConf) return;
    CChartData data;
    if (data.Load(filename, false, &m_unicode, false)) {
        const LanguageData *lang = nullptr;
        auto dot = PathFindExtension(filename); //points to the dot or the terminating 0 char
        if (*dot) {
            std::string ext = (const char*)AsUTF8(CString(dot+1));
            auto lang_name = pConf->m_languages.LanguageForExtension(ext);
            if (lang_name.length())
                lang = pConf->m_languages.GetLanguage(lang_name);
        }
        CChartData to_insert(*m_itrEditing);
        restartEditor = m_ExternalEditor.IsRunning();
        if (restartEditor)
            m_ExternalEditor.Stop(STOPEDITOR_WAIT);
        if (insert_at_cursor) {
            if (lang!=pConf->m_lang && IDNO == MessageBox(nullptr,
                _T("The language of the autosaved file is different from the current chart language. Proceed with insertion?"),
                _T("Msc-generator"), MB_YESNO)) {
                if (restartEditor)
                    m_ExternalEditor.Start();
                return;
            }
            theApp.m_pWndEditor->m_ctrlEditor.ReplaceSel(AsUnicode(data.GetText()));
        } else {
            //set language if it differs from the one we have
            if (lang!=pConf->m_lang)
                pConf->SetLanguage(lang);
            to_insert.Set(data.GetText());
            InsertNewChart(to_insert);
            //Copy text to the internal editor
            UpdateInternalEditor();
        }
        if (restartEditor)
            m_ExternalEditor.Start();
        //delete the file if we wanted to
        if (GetConf()->m_bRecoveryDeletes) {
            try {
                CFile::Remove(filename);
            }
            catch (CFileException *e) {
                //just warn
                pMainWnd->MessageBox(_T("For some reason I could not delete recovered file."), _T("Msc-generator Error"), MB_OK | MB_ICONWARNING);
            }
        }
        pMainWnd->ActivateHomeCategory(); //activate the home category (and hide the recovered file list)
        return; //we are done here
    }
    //signal error - could not read
    pMainWnd->MessageBox(_T("Recovery failed: Could not read recovered file."), _T("Msc-generator Error"), MB_OK | MB_ICONERROR);
    //re-create the list
    if (!pMainWnd->m_wndAutoSaveFiles.CollectAutoSavedFiles(
            theApp.GetDataRecoveryHandler()->GetAutosavePath(),
            theApp.GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE) ? pConf->m_lang : nullptr))
        pMainWnd->ActivateHomeCategory(); //activate the home category (and hide the recovered file list) if no entries left
#endif
}

/** Start the animation callback timer.
 * If a view already owns a timer do nothing. Else
 * start a timer at the first view.*/
void CMscGenDoc::StartFadingTimer()
{
	POSITION pos = GetFirstViewPosition();
	while(pos)
		if (m_pViewFadingTimer == GetNextView(pos)) return;
	//No timer
	pos = GetFirstViewPosition();
	CMscGenView *pView = dynamic_cast<CMscGenView *>(GetNextView(pos));
	if (pView)
		pView->StartFadingTimer();
}

/** Perform one step in all animation elements.
 * Make appearing ones more visible, fading ones less visible
 * and decrement time left before auto-fading for ones that are completely showing.
 * Remove the ones that have disappeared entirely. Invalidate all views (repaint)
 * @returns true if we have animation elements left. False if all have disappeared.*/
//return false if no fading rect remained and we need to maintain timer
bool CMscGenDoc::DoFading()
{
    if (m_animations.size()==0)
        return false;
    for (auto ta = m_animations.begin(); ta != m_animations.end(); /*nope*/) {
        if (ta->fade_value == ta->target_fade) {
            if (ta->fade_value>0) { //we are showing stabile
                if (ta->fade_delay>0)
                    ta->fade_delay = std::max(0, ta->fade_delay-FADE_TIMER_INTERVAL);
                if (ta->fade_delay==0 && !ta->keep)
                    ta->target_fade = 0; //start actual fading
                ta++;
                continue;
            }
            // else (fade_value==0) fallthrough
        } else if (ta->fade_value < ta->target_fade) {//APPEARING
            ta->fade_value += double(FADE_TIMER_INTERVAL)/ta->appe_time;
            if (ta->fade_value >= ta->target_fade)
                ta->fade_value = ta->target_fade;
            ta++;
            continue;
        } else {// FADING:
            ta->fade_value -= double(FADE_TIMER_INTERVAL)/ta->disa_time;
            if (ta->fade_value < ta->target_fade)
                ta->fade_value = ta->target_fade;
            ta++;
            continue;
        }
        //we have completely faded out
        //if a control, remove from m_controlsShowing
        if (ta->what==AnimationElement::CONTROL)
            for (auto ii = m_controlsShowing.begin(); ii!=m_controlsShowing.end(); /*nope*/)
                if (ii->second == ta->arc) m_controlsShowing.erase(ii++);
                else ii++;
        //erase animation
        ta = m_animations.erase(ta);
    }
	POSITION pos = GetFirstViewPosition();
	while(pos) {
		CMscGenView* pView = dynamic_cast<CMscGenView*>(GetNextView(pos));
		if (pView) pView->Invalidate();
	}
    CFrameWnd *pWnd = dynamic_cast<CFrameWnd *>(AfxGetMainWnd());
    if (pWnd) {
        //if we have a compiling animation, which is active (target_fade>0) we copy
        //this to the taskbar
        if (m_animations.end()!=std::find_if(m_animations.begin(), m_animations.end(),
            [](auto &ta) {return ta.what==AnimationElement::COMPILING_GREY && ta.target_fade; }))
            pWnd->SetProgressBarPosition(int(m_Progress->GetPercentage()*1000));
    }

	return m_animations.size()>0;
}


/** Add a tracking element to the list.
 * We check if this element is already part of the list and ignore if yes
 * (perhaps we lengthten its life or bring back to appearing if it already were
 * fading).
 * Starts the animation timer if not yet running.
 * Updates Views if needed & rets ture if so was needed.
 * @param [in] type Type of the animation element
 * @param [in] arc The arc corresponding to the animation (if any).
 * @param [in] fade_target The fading value [0..1] towards which the element will
 *                         animate. If not zero, then when reaching it, we will
 *                         start a 'delay' long wait and then set the target to zero.
 *                         If zero, when reaching it, we delete the animation element.
 * @param [in] starting_fade The fading value [0..1] we start the animantion from.
 *                           Used only in case we insert a new animation - for existing
 *                           ones we keep the current value.
 * @param [in] delay How many milliseconds the element shall be completely
 *                   shown after appearing, before it starts to auto-fade.
 *                   if <0 the element remains indefinitely after appearing.
 *                   (Will probably get killed by StartFadingAll().)
 * @returns The address of the added/updated animation. Valid only till the next
 *          animation operation. (Based on a vector iterator.)*/
AnimationElement* CMscGenDoc::_AddAnimationElement(AnimationElement::ElementType type,
                                                  Element *arc,
                                                  double fade_target, double starting_fade,
                                                  int delay)
{
    if (m_pCompilingThread) return nullptr;
    //sanitize input & find drawing area
    const Contour *draw;
    switch (type) {
    case AnimationElement::COMPILING_GREY:
        _ASSERT(arc==nullptr);
        arc = nullptr;
        draw = nullptr;
        break;
    case AnimationElement::FALLBACK_IMAGE:
        _ASSERT(arc==nullptr);
        arc = nullptr;
        if (m_fallback_image_location.IsEmpty())
            return nullptr;
        draw = &m_fallback_image_location;
        break;
    case AnimationElement::CONTROL:
    case AnimationElement::TRACKRECT:
        //Do not add if it has no visual element
        if (arc==nullptr) return nullptr;
        draw = &arc->GetAreaToDraw();
        if (draw->IsEmpty())
            return nullptr;
        break;
    default:
        _ASSERT(0);
    }

    auto i = std::find_if(m_animations.begin(), m_animations.end(),
        [&type, &arc](auto &a) {return a.what==type && a.arc==arc; });

    //We always redraw all the tracked rectangles, to look better
	if (i==m_animations.end()) {
        if (arc)
            m_animations.push_back(AnimationElement(arc, type, delay));
         else
            m_animations.push_back(AnimationElement(type, delay));
        i = --m_animations.end();
        if (type == AnimationElement::CONTROL && arc)
            m_controlsShowing[arc->GetControlLocation()] = arc;
        i->fade_value = std::max(0., std::min(1., starting_fade));
    }
    i->target_fade = std::max(0., std::min(1., fade_target));
    if (delay>=0)
        i->fade_delay = delay;
    POSITION pos = GetFirstViewPosition();
	while(pos) {
		CMscGenView* pView = dynamic_cast<CMscGenView*>(GetNextView(pos));
		if (pView) pView->Invalidate();
	}
    StartFadingTimer();
	return &*i;
}

/** Start a special animation to highlight fallback image locations.
 * @param [in] c The contours of the fallback images.*/
void CMscGenDoc::StartTrackFallbackImageLocations(const Contour &c)
{
    m_fallback_image_location = c;
    AddAnimationFallbackImage();
    StartFadingAll();
}


/**Start the fading process for all animation elements.
 * We do this even for elements that would not auto-fade by themselves.
 * If the 'keep' member of an element is set, we do not fade it, as if
 * it were listed in 'except'.
 * @param [in] type We start fading only animations of this type
 * @param [in] except If non-nullptr, we start fading animations corresponding to all
 *                    arcs except this one.*/
void CMscGenDoc::StartFadingAll(AnimationElement::ElementType type, const Element *except)
{
    if (m_pCompilingThread) return;
    for (auto i = m_animations.begin(); i!=m_animations.end(); i++)
        if ((type == i->what && i->arc == except) || i->keep)
            continue;
        else
            i->target_fade = 0;
	//If fading in progress we exit
	POSITION pos = GetFirstViewPosition();
	while(pos)
		if (m_pViewFadingTimer == GetNextView(pos)) return;
	//else we start fading immediately
	if (DoFading())
        StartFadingTimer();
}

bool CMscGenDoc::HasAnimationWithKeep()
{
    return
        std::find_if(m_animations.begin(), m_animations.end(),
            [](const auto &a) {return a.keep; })!=m_animations.end();
}

/** Resets the 'keep' member of all animations.
 * They will now start fading when StartFadingAll() is called.*/
void CMscGenDoc::ResetAllAnimationKeep()
{
    for (auto &a: m_animations)
        a.keep = false;
}


/** Enter or leave track mode.*/
void CMscGenDoc::SetTrackMode(bool on)
{
	if (on == m_bTrackMode) return;
    if (m_pCompilingThread && on) return;
	m_bTrackMode = on;
	if (on) {
        StartFadingAll(); //Delete trackrects from screen (even if turned on)
        CompileEditingChart(false, true, false);
        //We have already saved the internal editor selection state into m_saved_charrange in MscGenView::OnLButtonUp
	} else {
        ResetAllAnimationKeep();
        StartFadingAll();
#ifndef SHARED_HANDLERS
        if (IsInternalEditorVisible()) {
            //Disable selection change events - we are causing selection change
            //and we do not want to be notified (and add more track rects)
            DWORD eventmask = theApp.m_pWndEditor->m_ctrlEditor.GetEventMask();
            theApp.m_pWndEditor->m_ctrlEditor.SetEventMask(eventmask & ~ENM_SELCHANGE);
            theApp.m_pWndEditor->m_ctrlEditor.SetSel(m_saved_charrange);
            theApp.m_pWndEditor->m_ctrlEditor.SetEventMask(eventmask);
            m_saved_charrange.cpMin = -1;
            theApp.m_pWndEditor->SetFocus();
        }
#endif
    }
}

/**Checks if we have moved to a new arc and if so, adds
 * appropriate animation for it.
 * Called from CMscGenView::HooverMouse(), OnLButtonDblClk(), OnLButtonUp().
 * In tracking mode, we add tracking rectangles/shapes, else we check for
 * element controls.
 * @param [in] mouse The coordinates of the mouse in Chart space (local to the current page)
 * @param [in] silent If false and we select an element defined not in the main file, we
 *                    give a beep.
 * @returns a pointer to a link, if we hoover over one. nullptr if not*/
const ISMapElement * CMscGenDoc::UpdateTrackRects(CPoint mouse, bool silent)
{
    if (m_pCompilingThread) return nullptr; //skip if compiling
	Element *arc = m_ChartShown.GetArcByCoordinate(mouse);
    //Start fading everything except the highlighted element under the cursor (if any)
    StartFadingAll(AnimationElement::TRACKRECT, arc);
    //re-add those controls where the element is not under mouse but the control itself is
    for (auto &c: m_controlsShowing)
        if (c.first.IsWithinBool(XY(mouse.x, mouse.y)))
            AddAnimationControl(c.second);
    //If we hovered over a new arc, add controls for that, too
    if (arc && arc->GetControls().size())
        AddAnimationControl(arc);
    //check if we are above a link
    const ISMapElement *e = m_ChartShown.GetLinkByCoordinate(mouse);
    //if we do not display tracking rectangles, exit
	if (!m_bTrackMode) return e;
    //Re-add tracking rectangle under cursor
    if (arc) {
        AnimationElement *a = AddAnimationTrackRect(arc);
        //Hoover animations just appear a bit, except if we have already
        //actually clicked on them or if they are disappearing
        if (!a->keep && a->fade_value < a->target_fade)
            a->target_fade = AnimationElement::TRACK_MODE_HOOVER_FADE_PERCENT/100.;
    }
	//If arc has not changed, do nothing
	if (arc == m_last_arc)
		return e;
	m_last_arc = arc;
	//Now update selection in internal editor
	if (!IsInternalEditorVisible()) return e;
#ifndef SHARED_HANDLERS
	CCshRichEditCtrl &editor = theApp.m_pWndEditor->m_ctrlEditor;
	DWORD eventmask = editor.GetEventMask();
	//Disable selection change events - we are causing selection change
	//and we do not want to be notified (and add more track rects)
	editor.SetEventMask(eventmask & ~ENM_SELCHANGE);
	if (arc) {
		//Store selection if there was no previous saved selection
        if (m_saved_charrange.cpMin==-1) editor.GetSel(m_saved_charrange);
		HighLightArc(arc, silent);
	} else {
		//restore selection to the one before tracking was initiated
		editor.SetSel(m_saved_charrange);
        m_saved_charrange.cpMin=-1;
	}
	editor.SetEventMask(eventmask);
#endif
    return e;
}


/** Highlights (selects) one arc in the internal editor.
 * @param [in] arc The element to highlight
 * @param [in] silent If false we give a beep if the arc was not defined
 *                    in the file open in the editor.*/
void CMscGenDoc::HighLightArc(const Element *arc, bool silent)
{
	if (!IsInternalEditorVisible() || !arc || arc->file_pos.IsInvalid()) return;
#ifndef SHARED_HANDLERS
    if (!m_ChartShown.IsErrorInFile(arc->file_pos.start) ||
        !m_ChartShown.IsErrorInFile(arc->file_pos.end)) {
        if (!silent)
            MessageBeep(MB_ICONASTERISK);
        return;
    }
    CCshRichEditCtrl &editor = theApp.m_pWndEditor->m_ctrlEditor;
	long start = editor.ConvertLineColToPos(arc->file_pos.start.line-1, arc->file_pos.start.col-1);
	long end = editor.ConvertLineColToPos(arc->file_pos.end.line-1, arc->file_pos.end.col);
	editor.SetRedraw(false);
	editor.SetSel(start, start);
	POINT scroll_pos;
	::SendMessage(editor.m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&scroll_pos);
	editor.SetSel(start, end);
	::SendMessage(editor.m_hWnd, EM_SETSCROLLPOS, 0, (LPARAM)&scroll_pos);
	editor.SetRedraw(true);
	editor.Invalidate();
#endif
}

/** Called by the view when an element control (collapse/expand) is clicked.
 * We do the collapse/expand action, add a new element to the undo buffer and recompile.
 * @param [in] arc The arc for which a control is clicked.
 * @param [in] t Which control has been clicked.*/
bool CMscGenDoc::OnControlClicked(Element *arc, EGUIControlType t)
{
    if (arc==nullptr || t==EGUIControlType::INVALID) return false;
    if (m_pCompilingThread) return false; //skip if compiling
    std::string new_state = m_ChartShown.ControlClicked(arc, t);
    if (new_state.length()==0 || new_state == m_ChartShown.m_GUIState) return false; // no change
    CChartData chart(*m_itrEditing);
    chart.block_undo = true;
    chart.m_GUIState = new_state;
	InsertNewChart(chart);
    CompileEditingChart(true, false, false);
    return true;
}

/** Called if we have changed to or from the Embedded Object ribbon category.
 * This should happen only for embedded objects.
 * If we switch to it, we set the cache type to EMF (so that the user sees
 * the chart rendered to WMF, as it would in the container document),
 * update the container document and do the fallback image animation in
 * ReDrawEMF().
 * If we switch away from the Embedded Object category, we set the cache back
 * to recording suface (faster, nicer), redraw and update.*/
void CMscGenDoc::OnActivateEmbeddedRibbonCategory(bool embedded)
{
    if (m_pCompilingThread) return;
    if (embedded) {
        m_ChartShown.SetCacheType(CDrawingChartData::CACHE_EMF);
        ReDrawEMF();
    } else {
        m_ChartShown.SetCacheType(CDrawingChartData::CACHE_RECORDING);
        m_ChartShown.CompileIfNeeded(); //just redraws recording
        UpdateAllViews(nullptr);
    }
}

/** Redraw the (assumedly EMF-cached) chart.
 * If a redraw is needed, we update the Object size and fallback image coverage
 * edits on the Embedded Object panel, do the fallback image animation and
 * update the container document.*/
void CMscGenDoc::ReDrawEMF()
{
    if (!m_ChartShown.CompileIfNeeded()) //just redraws EMF
        return; //if no redraw happened
    if (m_ChartShown.GetCacheType()!=CDrawingChartData::CACHE_EMF)
        return;
    StartTrackFallbackImageLocations(m_ChartShown.GetWMFFallbackImagePos());
#ifndef SHARED_HANDLERS
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
    if (pWnd) {
        const size_t size = m_ChartShown.GetWMFSize() + serialize_doc_overhead +
                            m_ChartShown.GetText().GetLength();
        const double total = double(m_ChartShown.GetSize().cx)*m_ChartShown.GetSize().cy;
        pWnd->FillEmbeddedPanel(size, 100*m_ChartShown.GetWMFFallbackImagePos().GetArea() / total);
    }
#endif
    UpdateAllViews(nullptr);
    NotifyChanged(); //This updates the view in the container app for embedded objects, but does not save
}


#ifdef SHARED_HANDLERS

// Support for thumbnails
void CMscGenDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
    // Modify this code to draw the document's data
    dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

    CString strText = _T("TODO: implement thumbnail drawing here");
    LOGFONT lf;

    CFont* pDefaultGUIFont = CFont::FromHandle((HFONT)GetStockObject(DEFAULT_GUI_FONT));
    pDefaultGUIFont->GetLogFont(&lf);
    lf.lfHeight = 36;

    CFont fontDraw;
    fontDraw.CreateFontIndirect(&lf);

    CFont* pOldFont = dc.SelectObject(&fontDraw);
    dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
    dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CMscGenDoc::InitializeSearchContent()
{
    CString strSearchContent;
    // Set search contents from document's data.
    // The content parts should be separated by ";"

    // For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
    SetSearchContent(strSearchContent);
}

void CMscGenDoc::SetSearchContent(const CString& value)
{
    //if (value.IsEmpty()) {
    //    RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
    //} else {
    //    CMFCFilterChunkValueImpl *pChunk = NULL;
    //    ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
    //    if (pChunk != NULL) {
    //        pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
    //        SetChunkValue(pChunk);
    //    }
    //}
}

#else

/** Global function to get the document object.*/
CMscGenDoc *GetMscGenDocument()
{
#ifdef SHARED_HANDLERS
    _ASSERT(0);
    return nullptr;
#else
    CFrameWnd *pMainWnd = dynamic_cast<CFrameWnd*>(theApp.GetMainWnd());
    ASSERT(pMainWnd!=nullptr);
    if (!pMainWnd || pMainWnd->GetActiveView() == nullptr) return nullptr;
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(pMainWnd->GetActiveView()->GetDocument());
    return pDoc;
#endif
}



#endif // SHARED_HANDLERS

