/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2023 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file flowcsh.h The declaration for the FlowCsh class.
* @ingroup libflow_files */

#ifndef Flow_CSH_H
#define Flow_CSH_H

#include "csh.h"
#include "flowstyle.h"

namespace flow {

/** Coloring for Flow. */
class FlowCsh : public Csh
{
public:
    explicit FlowCsh(Csh::FileListProc proc);
    ~FlowCsh() override = default;
    std::unique_ptr<Csh> Clone() const override { return std::make_unique<FlowCsh>(*this); }

    // Inherited via Csh
    void FillNamesHints() override;
    void ParseText(std::string&& input, int cursor_p, bool pedantic) override;

};

}; //namespace

#endif