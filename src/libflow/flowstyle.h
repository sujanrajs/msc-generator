/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2023 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file xxxstyle.h Flow specific style and context declarations.
* @ingroup libxxx_files */

#ifndef FLOW_STYLE_H
#define FLOW_STYLE_H

#include "style.h"

namespace flow {

enum class EDir {
    UP=0,
    DOWN=1,
    LEFT=2,
    RIGHT=3
};

enum class ELogicalDir
{
    NDIR_INV = 0,
    NDIR = 1,
    SDIR_INV = 2,
    SDIR = 3
};

constexpr bool IsHorizontal(EDir d) { return d>=EDir::LEFT; }
constexpr bool IsIncreasing(EDir d) { return unsigned(d)&1; }
constexpr EDir Opposite(EDir d) { return EDir(unsigned(d)^1); }
constexpr EDir operator-(EDir d) { return Opposite(d); }
constexpr ELogicalDir Opposite(ELogicalDir d) { return ELogicalDir(unsigned(d)^1); }
constexpr ELogicalDir operator-(ELogicalDir d) { return Opposite(d); }
constexpr XY EDirOffset(EDir d) { return d==EDir::UP ? XY(0, -1) : d==EDir::DOWN ? XY(0, 1) : d==EDir::LEFT ? XY(-1, 0) : XY(1, 0); }
constexpr XY BoxSideMidPoint(const Block &b, EDir dir) { return dir==EDir::UP ? XY(b.x.MidPoint(), b.y.from) : dir==EDir::DOWN ? XY(b.x.MidPoint(), b.y.till) : dir==EDir::LEFT ? XY(b.x.from, b.y.MidPoint()) : XY(b.x.till, b.y.MidPoint()); }
constexpr EDir ConvertDir(ELogicalDir ld, EDir sdir, EDir ndir) { return ld == ELogicalDir::NDIR_INV ? -ndir : ld==ELogicalDir::NDIR ? ndir : ld==ELogicalDir::SDIR_INV ? -sdir : sdir; }

/** Style for Flow charts.*/
class FlowStyle : public SimpleStyleWithArrow<TwoArrowHeads>
{
    friend class FlowCsh;
    friend class FlowContext;
    FlowStyle(EStyleType tt, EColorMeaning cm, EArcArrowType a,
             bool t, bool l, bool f, bool s, bool nu, bool shp, bool x); ///Control which components are used
    bool f_flow;
public:
    ///Add your own attributes
    FlowStyle(EStyleType tt = EStyleType::STYLE, EColorMeaning cm = EColorMeaning::NOHOW); //Has all the components, but is empty
    FlowStyle(const FlowStyle&) = default;
    FlowStyle(FlowStyle&&) = default;
    FlowStyle &operator=(const FlowStyle&) = default;
    FlowStyle &operator=(FlowStyle&&) = default;
    void Empty() override;
    bool IsEmpty() const noexcept override { return  SimpleStyleWithArrow<TwoArrowHeads>::IsEmpty(); }
    void MakeCompleteButText() override;
    Style &operator +=(const Style &toadd) override;
    bool AddAttribute(const Attribute &a, Chart *) override;
    void AttributeNames(Csh &csh) const override;
    bool AttributeValues(std::string_view attr, Csh &csh) const override;
};

/** Context for Graphs. Just a styleset and colors, really.*/
class FlowContext : public ContextBase<FlowStyle>
{
public:
    /** This constructor is used to create an empty context or one set to plain.
     * Used an initializing the first context at parse, when starting to record 
     * a design, storing a procedure or moving a newly defined design to the design 
     * store.
     * @param [in] f If true, the context contains a value for all styles and attributes (Full)
     * @param [in] p Tells us what components to observe and how to behave during parsing.
     * @param [in] t Tells us with what content to create the context. It should not be 'COPY
     * @param [in] l The first character of the context in the input file.*/
    FlowContext(bool f, EContextParse p, EContextCreate t, const FileLineCol &l) 
        : ContextBase<FlowStyle>(f, p, EContextCreate::CLEAR, l)
    { switch (t) {
        default: _ASSERT(0); FALLTHROUGH;
        case EContextCreate::PLAIN: Plain(); break;
        case EContextCreate::EMPTY: Empty(); break; 
        case EContextCreate::CLEAR: break;}}
    /** This constructor is used when a context needs to be duplicated due to 
     * the opening of a new scope. You can change the parse mode, e.g., when
     * parsing the not-selected branch of an ifthenelse.*/
    FlowContext(const FlowContext &o, EContextParse p, const FileLineCol &l) :
        ContextBase<FlowStyle>(o, p, l) {}
    /** Semi copy operator: copy only user-defined context content. If o is full, we overwrite us, else just merge.*/
    void ApplyContextContent(const FlowContext &o);
    /** Semi move operator: move only user-defined context content. If o is full, we overwrite us, else just merge.*/
    void ApplyContextContent(FlowContext &&o);
    void Empty() override;
    void Plain() override;
};

}; //namespace

#endif //FLOW_STYLE_H
