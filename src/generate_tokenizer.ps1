$BISON = "c:\cygwin64\bin\bison.exe"

$lib = $args[0]
$lines = $args[1]

rm ${lib}_parse_compile.*, ${lib}_parse_csh.*, ${lib}_lang_compile.*, ${lib}_lang_csh.*

.{
    gc preamble_compile.yy | select -First $lines
    gc ${lib}_lang.yy | select -Skip $lines
} | sc ${lib}_lang_compile.yy
.{
    gc preamble_csh.yy | select -First $lines
    gc ${lib}_lang.yy | select -Skip $lines
} | sc ${lib}_lang_csh.yy

& $BISON --header=${lib}_parser_compile.h --output ${lib}_parser_compile.cpp ${lib}_lang_compile.yy
& $BISON --header=${lib}_parser_csh.h --output ${lib}_parser_csh.cpp ${lib}_lang_csh.yy

(gc ${lib}_parser_compile.h) -replace "\""${lib}_lang_compile\.yy\""", """${lib}_lang.yy""" | sc ${lib}_parser_compile.h
(gc ${lib}_parser_compile.cpp) -replace """${lib}_lang_compile\.yy\""", """${lib}_lang.yy""" | sc ${lib}_parser_compile.cpp
(gc ${lib}_parser_csh.h) -replace """${lib}_lang_csh\.yy\""", """${lib}_lang.yy""" | sc ${lib}_parser_csh.h
(gc ${lib}_parser_csh.cpp) -replace """${lib}_lang_csh\.yy\""", """${lib}_lang.yy""" | sc ${lib}_parser_csh.cpp
