#include <type_traits>
#include <iostream>
#include "maphoon-lexer/includes.h"
#include "graph_lexer.h"
#include "graph_parser_compile.h"

int main() {
    const std::array scanners = {
        std::pair{&graph::build_classifier<graph_compile_tokentype>, "basic"},
        std::pair{&graph::build_classifier_html_string<graph_compile_tokentype>, "html_string"},
    };
    std::ofstream ofile("graph_tokenizer.h");
    for (auto [scanner, name] : scanners) {
        std::ostringstream out;
        lexing::printcode<char, graph_compile_tokentype>( "char", "int", {"graph", name},
            scanner(), out,
            [](std::ostream& out, char ch) {
                 unsigned char c = ch;
                 if (c=='\\' || c=='\'') out << "\'\\" << c << '\'';
                 else if (32 <= c && c <= 127) out << '\'' << c << '\'';
                 else out << (int)c; },
            [](std::ostream& out, graph_compile_tokentype S) { out << int(S) ; }
        );
        //Remove Q00 (and the following colon) if present only once
        //to avoid "unused label" warnings
        std::string ret = std::move(out).str();
        if (const size_t pos = ret.find("Q00")
            ; pos != ret.npos && ret.substr(pos, 4) == "Q00:" && ret.find("Q00", pos + 1) == ret.npos)
            ret.erase(pos, 4);

        ofile << ret;
    }
}