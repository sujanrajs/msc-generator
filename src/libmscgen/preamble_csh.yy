%define api.prefix {msc_csh_}
%param{msc::MscCsh &csh}

%param{msc::lexing_state &current_state}
%code requires { #define C_S_H_IS_COMPILED }
