/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2023 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file msc_parse_tools.h Utilities for parsing.
 * @ingroup libmscgen_files
 * This file is used if we do parsing for language and not for Color Syntax Highlight.
 * Need to include this for the right YYLTYPE */

#ifndef MSC_PARSE_TOOLS_H
#define MSC_PARSE_TOOLS_H

#include "parser_tools.h"
#include "msc.h"


namespace msc {

std::string_view ConvertEmphasisToBox(std::string_view style, const FileLineCol &loc, MscChart &msc);
std::pair<std::string, std::string>
    BeautifySyntaxError(std::string msg, std::string_view last_token);
std::string process_multiline_qstring(std::string_view s, FileLineCol loc);
}; //namespace


#endif //MSC_PARSE_TOOLS_H