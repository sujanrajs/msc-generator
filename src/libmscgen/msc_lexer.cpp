#include <type_traits>
#include <iostream>
#include "maphoon-lexer/includes.h"
#include "msc_lexer.h"
#include "msc_parser_compile.h"

int main() {
    const std::array scanners = {
        std::pair{lexing_state::basic, "basic"},
        std::pair{lexing_state::mscgen_compat, "mscgen_compat"},
        std::pair{lexing_state::mscgen_compat_box, "mscgen_compat_box"},
    };
    std::ofstream ofile("msc_tokenizer.h");
    for (auto [state, name] : scanners) {
        std::ostringstream out;
        lexing::printcode<char, token_state_symbol<msc_compile_tokentype>>(
            "char", "int_token_state_symbol", {"msc", name},
            msc::build_classifier<msc_compile_tokentype>(state), out,
            [](std::ostream& out, char ch) {
                 unsigned char c = ch;
                 if (c=='\\' || c=='\'') out << "\'\\" << c << '\'';
                 else if (32 <= c && c <= 127) out << '\'' << c << '\'';
                 else out << (int)c; },
            [](std::ostream& out, token_state_symbol<msc_compile_tokentype> S) { out << S; }
        );
        //Remove Q00 (and the following colon) if present only once
        //to avoid "unused label" warnings
        std::string ret = std::move(out).str();
        if (const size_t pos = ret.find("Q00")
            ; pos != ret.npos && ret.substr(pos, 4) == "Q00:" && ret.find("Q00", pos + 1) == ret.npos)
            ret.erase(pos, 4);

         ofile << ret;
    }
}