rem Build GLPK DLL with Microsoft Visual Studio Community 2017

rem NOTE: Make sure that HOME variable specifies correct path
set HOME="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\Tools"

call %HOME%\vsDevCmd.bat 
copy config_VC config.h
nmake.exe /f Makefile_VC_DLL
nmake.exe /f Makefile_VC_DLL check

pause
